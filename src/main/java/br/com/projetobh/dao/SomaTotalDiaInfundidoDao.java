package br.com.projetobh.dao;

import br.com.projetobh.model.SomaTotalDiaInfundido;

public interface SomaTotalDiaInfundidoDao {
	
	void save(SomaTotalDiaInfundido somaTotalDiaInfundido);
}
