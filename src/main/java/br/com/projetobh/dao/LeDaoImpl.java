package br.com.projetobh.dao;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.projetobh.model.LiquidoEliminado;
import br.com.projetobh.model.SomaTotalDia;
import br.com.projetobh.model.SomaTotalLEliminado;
import br.com.projetobh.model.SomatorioEli;

@Repository
public class LeDaoImpl extends AbstractDao<LiquidoEliminado, Long> implements LeDao{
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<LiquidoEliminado> findByData(LocalDate entrada) {
		String jpql = new StringBuilder("select l from LiquidoInfundido l ")
				.append("where l.dataCadastro = ?1 ")
				.append("order by l.dataCadastro asc")
				.toString();
		return createQuery(jpql, entrada);
	}

	@Override
	public List<LiquidoEliminado> findByHoras(LocalTime hora) {
		String jpql = new StringBuilder("select l from LiquidoInfundido l ")
				.append("where l.horaCadastro = ?1 ")
				.append("order by l.horaCadastro asc")
				.toString();
		return createQuery(jpql, hora);
	}
	
	public List<LiquidoEliminado> findBySm(){
		String jpql = new StringBuilder("select l.urina from LiquidoInfundido l ")
				.append("where l.turno LIKE 'MA")
				.toString();
		return createQuery(jpql);
		//SELECT urina FROM tbl_linfundido WHERE turno LIKE 'MA';
	}

	@Override
	public Double total() {
		Query query = em.createQuery("select sum(l.sm) from LiquidoInfundido l");
	    return (Double) query.getSingleResult();
	}
	
	
	
	/**
	 * select sum(urina) as soma_urina, sum(fezes) as soma_fezes, sum(vomito) as soma_vomito, sum(dreno) as soma_dreno, sum(estase) soma_estase
	 *  from tbl_leliminado where hora_cadastro between '07:00:00' and '23:00:00 and data_cadastro = '2019-10-27';
	 */	
	public SomatorioEli somatorioLiquidoEliminado(LocalTime horaInicio, LocalTime horaFim, LocalDate dataCadastro) {
		
		//select sum(urina) as soma_urina, sum(fezes) as soma_fezes, sum(vomito) as soma_vomito, sum(dreno) as dreno, 
		//sum(estase) as soma_estase from tbl_leliminado where hora_cadastro between '07:00:00' and '22:15:00' and '2019-10-27';

		String jpql = new StringBuilder("select new br.com.projetobh.model.SomatorioEli (sum(l.urina) as urina, ")
				.append(" sum(l.fezes) as fezes, ")
				.append(" sum(l.vomito) as vomito, ")
				.append(" sum(l.dreno) as dreno, ")
				.append(" sum(l.estase) as estase) ")
				.append(" from LiquidoEliminado l ")
				
				.append(" where l.horaCadastro between ?1 and ?2 ")
				.append(" and l.dataCadastro = ?3 ")
				.toString();
		return createQuerySomatorioEli(jpql, horaInicio, horaFim, dataCadastro);
	}
	
	/**
	 * ESSA É A CONSULTA QUE TEM QUE SER FEITA PARA SOMAR CADA LINHA DA CADA COLUNA POR HORARIO DA tbl_leliminado_somatorio
	 * select sum((dreno)+(estase)+(fezes)+(urina)+(vomito)) as total from tbl_linfundido_somatorio where hora_cadastro '23:50:00';
	 * 
	 * 
	 */
	public SomaTotalLEliminado somaTotalLEliminado(LocalTime hora, LocalDate dataCadastro) {
		String jpql = new StringBuilder("select new br.com.projetobh.model.SomaTotalLEliminado (sum((dreno)+(estase)+(fezes)+(urina)+(vomito))) ")
				.append(" from SomatorioEli l ")
				.append(" where l.horaCadastro = ?1 ")
				.append(" and l.dataCadastro = ?2 ")
				.toString();
		
		return createQuerySomaTotalEli(jpql, hora, dataCadastro);
	}
	
	
	/**
	 * ESSE CALCULO SERÁ FEITO ATE O DIA DA APRESENTAÇÃO DO PROJETO
	 * select (sum(soma_total_liquidos) - (select sum(soma_total_liquidos) from soma_totalleliminado)) as total from soma_totallinfundido where hora_cadastro = '22:42:00';
	 */
	
	
	/**
	 * Método para consultar o total do liquido em cada turno em 24:00 horas
	 *select sum(urina) as soma_urina_dia, sum(fezes) as soma_fezes, sum(vomito) as soma_vomito, sum(dreno) as dreno, 
	 *sum(estase) as soma_estase from tbl_liq_eliminado_somatorio where data_cadastro '2019-10-27'; 
	 * @param dataCadastro
	 * @return
	 */
	public SomaTotalDia somaTotalDia(LocalDate dataCadastro) {
		String jpql = new StringBuilder("select new br.com.projetobh.model.SomaTotalDia (sum(l.urina) as total_dia_urina, ")
				.append(" sum(l.fezes) as total_dia_fezes, ")
				.append(" sum(l.vomito) as total_dia_vomito, ")
				.append(" sum(l.dreno) as total_dia_dreno, ")
				.append(" sum(l.estase) as total_dia_estase) ")
				.append(" from SomatorioEli l ")
				.append(" where l.dataCadastro = ?1 ")
				.toString();
		return createQueryTotalDia(jpql, dataCadastro);
	}
	

}
