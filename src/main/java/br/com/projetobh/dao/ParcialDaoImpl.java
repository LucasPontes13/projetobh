package br.com.projetobh.dao;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.stereotype.Repository;

import br.com.projetobh.model.ParcialDoTurno;

@Repository
public class ParcialDaoImpl extends AbstractDao<ParcialDoTurno, Long> implements ParcialDao {
	
	
	/**
	 * select (sum(soma_total_liquidos) - (select sum(soma_total_liquidos) from soma_totalleliminado)) as total from soma_totallinfundido where hora_cadastro = '22:42:00';
	 */
	@Override
	public ParcialDoTurno somaParcialDoTurno(LocalTime hora, LocalDate dataCadastro) {
		String jpql = new StringBuilder("select new br.com.projetobh.model.ParcialDoTurno (sum(soma_total_liquidos) - (select sum(soma_total_liquidos) from soma_totalleliminado)) ")
						.append(" from soma_totallinfundido l ")
						.append(" where l.horaCadastro = ?1 ")
						.append(" and l.dataCadastro = ?2 ")
						.toString();
		return createQueryParcial(jpql, hora, dataCadastro);
	}

}
