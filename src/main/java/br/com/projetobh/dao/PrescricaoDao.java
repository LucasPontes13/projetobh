package br.com.projetobh.dao;

import java.util.List;

import br.com.projetobh.model.PrescricaoReceituario;

public interface PrescricaoDao {
	
	void save(PrescricaoReceituario prescricao);
	void update(PrescricaoReceituario prescricao);
	void delete(Long id);
	PrescricaoReceituario findById(Long id);
	List<PrescricaoReceituario> findAll();
}
