package br.com.projetobh.dao;

import br.com.projetobh.model.SomaTotalLEliminado;

public interface SomaTotalLEliminadoDao {
	void save(SomaTotalLEliminado somatorio);
}
