package br.com.projetobh.dao;

import java.util.List;

import br.com.projetobh.model.IdadeGestacional;

public interface IdadeGestacionalDao {
	void save(IdadeGestacional idadeGestacional);
	void update(IdadeGestacional idadeGestacional);
	void delete(Long id);
	IdadeGestacional findById(Long id);
	List<IdadeGestacional> findAll();
}
