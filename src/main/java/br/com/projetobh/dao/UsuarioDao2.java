package br.com.projetobh.dao;

import java.util.List;

import br.com.projetobh.model.Usuario;

public interface UsuarioDao2 {
	void save(Usuario usuario);

	void update(Usuario usuario);

	void delete(Long id);

	Usuario findById(Long id);

	List<Usuario> findAll();
}
