package br.com.projetobh.dao;

import java.util.List;

import br.com.projetobh.model.Paciente;

public interface PacienteDao {
	/**
	 * Assinatura dos métodos
	 * @param paciente
	 */
	void save(Paciente paciente);
	
	void update(Paciente paciente);
	
	void delete(Long id);
	
	Paciente findById(Long id);
	
	List<Paciente> findAll();
}
