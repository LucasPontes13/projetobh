package br.com.projetobh.dao;

import java.time.LocalDate;
import java.time.LocalTime;

import br.com.projetobh.model.ParcialDoTurno;

public interface ParcialDao {
	void save(ParcialDoTurno parcialDoTurno);
	
	ParcialDoTurno somaParcialDoTurno(LocalTime hora, LocalDate dataCadastro);
}
