package br.com.projetobh.dao;

import org.springframework.stereotype.Repository;

import br.com.projetobh.model.Medicamento;

@Repository
public class MedicamentoDaoImpl extends AbstractDao<Medicamento, Long> implements MedicamentoDao{

}
