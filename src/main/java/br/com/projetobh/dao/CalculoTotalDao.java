package br.com.projetobh.dao;

import java.util.List;

import br.com.projetobh.model.CalculoTotal;

public interface CalculoTotalDao {
	
	void save(CalculoTotal calculoTotal);
	
	List<CalculoTotal> findAll();
}
