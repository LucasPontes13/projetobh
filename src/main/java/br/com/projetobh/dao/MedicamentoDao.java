package br.com.projetobh.dao;

import java.util.List;

import br.com.projetobh.model.Medicamento;

public interface MedicamentoDao {
	void save(Medicamento medicamento);
	void update(Medicamento medicamento);
	void delete(Long id);
	Medicamento findById(Long id);
	List<Medicamento> findAll();
}
