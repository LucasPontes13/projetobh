package br.com.projetobh.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.projetobh.model.ParcialDoTurno;
import br.com.projetobh.model.SomaTotalDia;
import br.com.projetobh.model.SomaTotalDiaInfundido;
import br.com.projetobh.model.SomaTotalLEliminado;
import br.com.projetobh.model.SomaTotalLInfundido;
import br.com.projetobh.model.SomatorioEli;
import br.com.projetobh.model.SomatorioInf;

/**
 * Esse classe vai funcionar como uma classe generica 
 * DaoGeneric - É uma classe que fornece vários métodos comuns á varias classes
 * @author Harmfull
 *
 * @param <T> representa a entida
 * @param <PK> representa o tipo de chave primaria
 */
public abstract class AbstractDao<T, PK extends Serializable> {
	
	@SuppressWarnings("unchecked")
	private final Class<T> entityClass =
			(Class<T>) ( (ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	
	@PersistenceContext
	private EntityManager entityManager;
	
	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	public void save(T entity) {
		entityManager.persist(entity);
	}
	
	public void update(T entity) {
		entityManager.merge(entity);
	}
	
	public void delete(PK id) {
		entityManager.remove(entityManager.getReference(entityClass, id));
	}
	
	public T findById(PK id) {
		return entityManager.find(entityClass, id);
	}
	
	public List<T> findAll(){
		return entityManager
				.createQuery("from " + entityClass.getSimpleName(), entityClass)
				.getResultList();
	}
	
	/**
	 * É protected porque ela vai ser usado a partir de heranca, no máximo até as classes 
	 * que herdan ela, mais nenhuma
	 * @param jpql
	 * @param params
	 * @return
	 */
	protected List<T> createQuery(String jpql, Object... params){
		TypedQuery<T> query = entityManager.createQuery(jpql, entityClass);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i+1, params[i]);
		}
		return query.getResultList();
	}
	
	
	/**
	 * Método que realizará a cosnulta dos valores na tabela tbl_lnfundido 
	 * @param jpql
	 * @param params
	 * @return
	 */
	protected SomatorioInf createQuerySomatorioInf(String jpql, Object... params){
		TypedQuery<SomatorioInf> query = entityManager.createQuery(jpql, SomatorioInf.class);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i+1, params[i]);
		}
		return (SomatorioInf) query.getSingleResult();
	}
	
	/**
	 * Método que realizará a cosnulta dos valores na tabela tbl_leliminado
	 * @param jpql
	 * @param params
	 * @return
	 */
	protected SomatorioEli createQuerySomatorioEli(String jpql, Object... params){
		TypedQuery<SomatorioEli> query = entityManager.createQuery(jpql, SomatorioEli.class);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i+1, params[i]);
		}
		return (SomatorioEli) query.getSingleResult();
	}
	
	
	/**
	 * Método SomaTotalLInfundido para realizar a consulta dos valores na tabela
	 * @param jpql
	 * @param params
	 * @return
	 */
	protected SomaTotalLInfundido createQuerySomaTotal(String jpql, Object... params){
		TypedQuery<SomaTotalLInfundido> query = entityManager.createQuery(jpql, SomaTotalLInfundido.class);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i+1, params[i]);
		}
		return (SomaTotalLInfundido) query.getSingleResult();
	}
	
	protected SomaTotalLEliminado createQuerySomaTotalEli(String jpql, Object... params){
		TypedQuery<SomaTotalLEliminado> query = entityManager.createQuery(jpql, SomaTotalLEliminado.class);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i+1, params[i]);
		}
		return (SomaTotalLEliminado) query.getSingleResult();
	}
	
	/**
	 * Método para montar a jpql do calculo parcial de cada turno
	 * @param jpql
	 * @param params
	 * @return
	 */
	protected ParcialDoTurno createQueryParcial(String jpql, Object... params){
		TypedQuery<ParcialDoTurno> query = entityManager.createQuery(jpql, ParcialDoTurno.class);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i+1, params[i]);
		}
		return (ParcialDoTurno) query.getSingleResult();
	}
	
	
	/**
	 * Metodo para consulta para soma de 24h do liquido eliminado
	 * @param jpql
	 * @param params
	 * @return
	 */
	protected SomaTotalDia createQueryTotalDia(String jpql, Object... params){
		TypedQuery<SomaTotalDia> query = entityManager.createQuery(jpql, SomaTotalDia.class);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i+1, params[i]);
		}
		return (SomaTotalDia) query.getSingleResult();
	}
	
	/**
	 * Método para consulta para soma de 24h do liquido infundido
	 * @param jpql
	 * @param params
	 * @return
	 */
	protected SomaTotalDiaInfundido createQueryTotalDiaInfundido(String jpql, Object... params){
		TypedQuery<SomaTotalDiaInfundido> query = entityManager.createQuery(jpql, SomaTotalDiaInfundido.class);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i+1, params[i]);
		}
		return (SomaTotalDiaInfundido) query.getSingleResult();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
