package br.com.projetobh.dao;

import org.springframework.stereotype.Repository;

import br.com.projetobh.model.Paciente;
/**
 * Bean PacienteDaoImpl 
 * Caso necessite incluir um novo método, ele será incluido nessa classe
 * e adicionar a assinatura desse método lá na interface PacienteDao
 * @author Harmfull
 *
 */
@Repository
public class PacienteDaoImpl extends AbstractDao<Paciente, Long> implements PacienteDao{

}
