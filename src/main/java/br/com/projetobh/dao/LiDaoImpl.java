package br.com.projetobh.dao;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.projetobh.model.LiquidoInfundido;
import br.com.projetobh.model.SomaTotalDia;
import br.com.projetobh.model.SomaTotalDiaInfundido;
import br.com.projetobh.model.SomaTotalLInfundido;
import br.com.projetobh.model.SomatorioInf;

@Repository
public class LiDaoImpl extends AbstractDao<LiquidoInfundido, Long> implements LiDao{
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<LiquidoInfundido> findByData(LocalDate entrada) {
		String jpql = new StringBuilder("select l from LiquidoInfundido l ")
				.append("where l.dataCadastro = ?1 ")
				.append("order by l.dataCadastro asc")
				.toString();
		return createQuery(jpql, entrada);
	}

	@Override
	public List<LiquidoInfundido> findByHoras(LocalTime hora) {
		String jpql = new StringBuilder("select l from LiquidoInfundido l ")
				.append("where l.horaCadastro = ?1 ")
				.append("order by l.horaCadastro asc")
				.toString();
		return createQuery(jpql, hora);
	}
	
	public List<LiquidoInfundido> findBySm(){
		String jpql = new StringBuilder("select l.sm from LiquidoInfundido l ")
				.append("where l.turno LIKE 'MA'")
				.toString();
		return createQuery(jpql);
		//SELECT soro FROM tbl_linfundido WHERE turno LIKE 'MA';
	}

	/**
	 * Este método vai buscar o valor de cada atributo da classe LiquidoInfundido no banco de dados
	 * @return 
	 */
//	@Override
//	public List<LiquidoInfundido> findByValores(double sm) {
//		String jpql = new StringBuilder("select sm from tbl_linfundido ")
//				.toString();
//		return createQuery(jpql, sm);
//	}

	//vai retornar a somatoria dos valores armazenados no banco de dados
//	@Override
//	public Double total() {
//		CriteriaBuilder builder = em.getCriteriaBuilder();
//		CriteriaQuery<Double> criteria = builder.createQuery(Double.class);
//		Root<LiquidoInfundido> root = criteria.from(LiquidoInfundido.class);
//		Path<Double> valor = root.get("sm");
//		Expression<Double> soma = builder.sum(valor);
//		criteria.select(soma);
//		TypedQuery<Double> query = em.createQuery(criteria);
//		Double total = query.getSingleResult();
//		return total;
//	}
	
//	@Override
//	public Double total(){
//	    Query query = em.createQuery("select sum(l.sm) from LiquidoInfundido l");
//	    return (Double) query.getSingleResult();
//	  }
	
	
	/*
	rodar SQL no banco para validar o retorno
	select sum(npt) as soma_npt, sum(flush) as soma_flush, sum(hemo) as soma_hemo, sum(sm) soma_sm, sum(sonda_lm_la) soma_sonda_lm_la, sum(soro) soma_soro, sum(vo_lm_la) soma_vo_lm_la from tbl_linfundido where hora_cadastro between '07:00:00' and '23:00:00' and data_cadastro = '2019-10-15';
	 */
	public SomatorioInf somatorioLiquidoInfundido(LocalTime horaInicio, LocalTime horaFim, LocalDate dataCadastro) {
		String jpql = new StringBuilder("select new br.com.projetobh.model.SomatorioInf (sum(l.npt) as ntp, ")
				.append(" sum(l.flush) as flush, ")
				.append(" sum(l.hemo) as hemo, ")
				.append(" sum(l.sm) as sm, ")
				.append(" sum(l.sondaLmLa) as sondaLmLa, ")
				.append(" sum(l.soro) as soro, ")
				.append(" sum(l.voLmLa) as voLmLa)")
				.append(" from LiquidoInfundido l ")

				.append(" where l.horaCadastro between ?1 and ?2 ")
				.append(" and l.dataCadastro = ?3 ")
				.toString();
		return createQuerySomatorioInf(jpql, horaInicio, horaFim, dataCadastro);
	}
	
	
	/**
	 * Precisa montar um classe para salvar o total do balanco hidrico
	 * ESSA É A CONSULTA QUE TEM QUE SER FEITA PARA SOMAR CADA LINHA DA CADA COLUNA POR HORARIO DA tbl_leliminado_somatorio
	 * select sum((flush)+(hemo)+(npt)+(sm)+(sonda_lm_la)+(soro)+(vo_lm_la)) as total from tbl_leliminado_somatorio where hora_cadastro = '23:50:00' and data_cadastro = ';
	 */	
	public SomaTotalLInfundido somaTotalLInfundido(LocalTime hora, LocalDate dataCadastro) {
		String jpql = new StringBuilder("select new br.com.projetobh.model.SomaTotalLInfundido (sum((flush)+(hemo)+(npt)+(sm)+(sonda_lm_la)+(soro)+(vo_lm_la))) ")
				.append(" from SomatorioInf l ")
				.append(" where l.horaCadastro = ?1 ")
				.append(" and l.dataCadastro = ?2 ")
				.toString();
		
		return createQuerySomaTotal(jpql, hora, dataCadastro);
	}
	
	
	/**
	 * Método para consultar o total do liquido em cada turno em 24:00 horas
	 * @param dataCadastro
	 * @return
	 */
	public SomaTotalDiaInfundido somaTotalDiaInfundido(LocalDate dataCadastro) {
		String jpql = new StringBuilder("select new br.com.projetobh.model.SomaTotalDiaInfundido (sum(l.npt) as total_dia_npt, ")
				.append(" sum(l.flush) as total_dia_flush, ")
				.append(" sum(l.hemo) as total_dia_hemo, ")
				.append(" sum(l.sm) as total_dia_sm, ")
				.append(" sum(l.sondaLmLa) as total_dia_sondaLmLa, ")
				.append(" sum(l.soro) as total_dia_soro, ")
				.append(" sum(l.voLmLa) as total_dia_voLmLa) ")
				.append(" from SomatorioInf l ")
				.append(" where l.dataCadastro = ?1 ")
				.toString();
		return createQueryTotalDiaInfundido(jpql, dataCadastro);
	}
	
}
