package br.com.projetobh.dao;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import br.com.projetobh.model.LiquidoInfundido;
import br.com.projetobh.model.SomaTotalDiaInfundido;
import br.com.projetobh.model.SomaTotalLInfundido;
import br.com.projetobh.model.SomatorioInf;

public interface LiDao {
	void save(LiquidoInfundido liquidoInfundido);

	void update(LiquidoInfundido liquidoInfundido);

	void delete(Long id);

	LiquidoInfundido findById(Long id);

	List<LiquidoInfundido> findAll();
	
	List<LiquidoInfundido> findByData(LocalDate entrada);

	List<LiquidoInfundido> findByHoras(LocalTime hora);
	
	List<LiquidoInfundido> findBySm();
	
	SomatorioInf somatorioLiquidoInfundido(LocalTime horaInicio, LocalTime horaFim, LocalDate dataCadastro);
	
	SomaTotalLInfundido somaTotalLInfundido(LocalTime hora, LocalDate dataCadastro);
	
	SomaTotalDiaInfundido somaTotalDiaInfundido(LocalDate dataCadastro);

}
