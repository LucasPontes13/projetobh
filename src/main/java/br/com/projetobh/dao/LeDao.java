package br.com.projetobh.dao;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import br.com.projetobh.model.LiquidoEliminado;
import br.com.projetobh.model.SomaTotalDia;
import br.com.projetobh.model.SomaTotalLEliminado;
import br.com.projetobh.model.SomatorioEli;

public interface LeDao {
	void save(LiquidoEliminado liquidoEliminado);

	void update(LiquidoEliminado liquidoEliminado);

	void delete(Long id);

	LiquidoEliminado findById(Long id);

	List<LiquidoEliminado> findAll();

	List<LiquidoEliminado> findByData(LocalDate entrada);

	List<LiquidoEliminado> findByHoras(LocalTime hora);

	SomatorioEli somatorioLiquidoEliminado(LocalTime horaInicio, LocalTime horaFim, LocalDate dataCadastro);
	
	SomaTotalLEliminado somaTotalLEliminado(LocalTime hora, LocalDate dataCadastro); 
	
	SomaTotalDia somaTotalDia(LocalDate dataCadastro);

	Double total();
}
