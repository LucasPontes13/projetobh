package br.com.projetobh.controller;

import java.util.List;

import javax.validation.Valid;

import br.com.projetobh.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.projetobh.model.BalancoHidrico;
import br.com.projetobh.model.IdadeGestacional;
import br.com.projetobh.model.Paciente;
import br.com.projetobh.model.Setor;

@Controller
@RequestMapping("/pacientes")
public class PacienteController {
	
	//variavel de instancia 
	//injetando com o @Autowired
	@Autowired
	private PacienteService service;
	
	@Autowired
	private SetorService setorService;
	
	@Autowired
	private BalancoHidricoService bhService;
	
	//Método GET para buscar a página html
	//vai abrir a página de cadastrar paciente 
	@GetMapping("/cadastrar")
	public String cadastrar(Paciente paciente) {
		return "/paciente/cadastro";
	}
		
	//Método POST para salvar os dados do formulario
	@PostMapping("/salvar")  
	public String salvar(@Valid Paciente paciente, BindingResult result, RedirectAttributes attr) {
		
		if(result.hasErrors()) {
			return "/paciente/cadastro";// ai so realiza o retorno para a pagina
		}
		
		service.salvar(paciente);		
		attr.addFlashAttribute("success", "Paciente salvo com sucesso.");
		return "redirect:/pacientes/cadastrar";
	}
	
	@GetMapping("/listar")
	public String listar(ModelMap model) {
		model.addAttribute("pacientes", service.buscarTodos());
		return "/paciente/lista";
	}
	
	// Método que recebe a solicitação do botão de editar
	@GetMapping("/editar/{id}")
	public String preEditar(@PathVariable("id") Long id, ModelMap model) {
		model.addAttribute("paciente", service.buscarPorId(id));
		return "/paciente/cadastro";
	}

	@PostMapping("/editar")
	public String editar(@Valid Paciente paciente, BindingResult result, RedirectAttributes attr) {
		
		if (result.hasErrors()) {
			return "/paciente/cadastro";// ai so realiza o retorno para a pagina
		}
		
		service.editar(paciente);
		attr.addFlashAttribute("success", "Paciente editado com sucesso.");
		return "redirect:/pacientes/cadastrar";
	}
	
	/**
	 * PRECISA VERIFICAR SE O PACIENTE TEM BALANCO HIDRICO, SE TIVER, ELE NÃO PODE SER EXCLUIDO
	 * 
	 */
	@GetMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, ModelMap model) {

		if (service.pacienteTemBalanco(id)) {
			model.addAttribute("fail", "Paciente não removido. possui balanco(s) vinculado(s).");
		} else {
			service.excluir(id);
			model.addAttribute("success", "Paciente excluído com sucesso.");
		}

		// outro tipo de redirect
		// chama o método listar passando o um modelMap que ira para o método
		// listar que vai buscar a lista de setores cadastrados
		return listar(model);
	}
	
	//metodo para buscar todos os setores cadastrados
	@ModelAttribute("setores")
	public List<Setor> listaSetores(){
		return setorService.buscarTodos();
	}

}
