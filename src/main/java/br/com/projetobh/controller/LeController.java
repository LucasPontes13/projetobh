package br.com.projetobh.controller;

import java.util.List;

import br.com.projetobh.service.BalancoHidricoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.projetobh.model.BalancoHidrico;
import br.com.projetobh.model.LiquidoEliminado;
import br.com.projetobh.model.TURNO;
import br.com.projetobh.service.BhService;
import br.com.projetobh.service.LeService;

@Controller
@RequestMapping("/eliminados")
public class LeController {
	
	@Autowired
	private LeService service;
	
	@Autowired
	private BalancoHidricoService bhService;
	
	@GetMapping("/cadastrar")
	public String cadastrar(LiquidoEliminado liquidoEliminado) {
		return "/liquidoEliminado/cadastro";
	}
	
	@PostMapping("/salvar")
	public String salvar(LiquidoEliminado liquidoEliminado, RedirectAttributes attr) {
		service.salvar(liquidoEliminado);
		attr.addFlashAttribute("success", "Liquidos Eliminados salvos com sucesso.");
		return "redirect:/eliminados/cadastrar";
	}
	
	@ModelAttribute("balancos")
	public List<BalancoHidrico> listaBalancos(){
		return bhService.buscarTodos();
	}
	
	@ModelAttribute("turnos")
	public TURNO[] getTurnos() {
		return TURNO.values();
	}
}
