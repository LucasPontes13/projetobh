package br.com.projetobh.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import br.com.projetobh.service.BalancoHidricoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.projetobh.model.BalancoHidrico;
import br.com.projetobh.model.LiquidoInfundido;
import br.com.projetobh.model.TURNO;
import br.com.projetobh.service.BhService;
import br.com.projetobh.service.LeService;
import br.com.projetobh.service.LiService;

@Controller
@RequestMapping("/infundidos")
public class LiController {
	
	@Autowired
	private LiService service;
	
	@Autowired
	private LeService leService;
	
	@Autowired
	private BalancoHidricoService bhService;
	
	@GetMapping("/cadastrar")
	public String cadastrar(LiquidoInfundido liquidoInfundido) {
		return "/liquidoInfundido/cadastro";
	}
	
	@PostMapping("/salvar")
	public String salvar(LiquidoInfundido liquidoInfundido, RedirectAttributes attr) {
		service.salvar(liquidoInfundido);
		attr.addFlashAttribute("success", "Liquidos Infundidos salvos com sucesso.");
		return "redirect:/infundidos/cadastrar";
	}
	
	//Traz as informações na tabela para calculoParcial
	@GetMapping("/listar")
	public String listarCalculo(ModelMap model) {
		model.addAttribute("infundidos", service.buscarTodos());//busca os dados dos liquidos infundidos
		//model.addAttribute("eliminados", leService.buscarTodos());// busca os dados dos liquidos eliminados
		return "/tabelaParaCalculoParcial/calculoParcial";// renderiza a página 
	}
	
	//Método executado a partir do batão de pesquisa para buscar os liquidos daquele dia para o calculo parcial 
	@GetMapping("/buscar/data")
	public String getPorDatas(@RequestParam("entrada") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
							LocalDate entrada, ModelMap model) {
		model.addAttribute("infundidos", service.buscarPorDatas(entrada));
		return "/tabelaParaCalculoParcial/calculoParcial";
	}
	
	//Método executado a partir do batão de pesquisa "horario" para buscar os liquidos daquele horario para o calculo parcial
	@GetMapping("/buscar/hora")
	public String getPorHora(@RequestParam("hora") @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
							LocalTime hora, ModelMap model) {
		model.addAttribute("infundidos", service.buscarPorHoras(hora));
		return "/tabelaParaCalculoParcial/calculoParcial";
	}
	
	/**
	 * criar uma página especifica para trazer a parcial 
	
	
	@GetMapping("/somar")
	public String getListar(@RequestParam("soma") ModelMap model, ArrayList<LiquidoInfundido> lista) {
		model.addAttribute("infundidos", service.total(lista));//busca os dados dos liquidos infundidos
		return "/tabelaParaCalculoParcial/calculoParcial";// renderiza a página    
	}*/
	
	//
	@ModelAttribute("balancos")
	public List<BalancoHidrico> listaBalancos(){
		return bhService.buscarTodos();
	}
	
	@ModelAttribute("turnos")
	public TURNO[] getTurnos() {
		return TURNO.values();
	}
	
	@GetMapping("/buscar/somatorio")
	public String getSomatorio(@RequestParam("horaInicio") @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
							LocalTime horaInicio, @RequestParam("horaFim") @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
							LocalTime horaFim, @RequestParam("dataCadastro") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
			LocalDate dataCadastro, ModelMap model) {
		model.addAttribute("infundidos", service.parcial(horaInicio, horaFim, dataCadastro));
		return "/tabelaParaCalculoParcial/calculoParcial";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
