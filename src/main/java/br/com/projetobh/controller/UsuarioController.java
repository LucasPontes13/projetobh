package br.com.projetobh.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.projetobh.model.Perfil;
import br.com.projetobh.model.Usuario;
import br.com.projetobh.service.UsuarioService;
import br.com.projetobh.service.UsuarioService2;

@Controller
@RequestMapping("u")
public class UsuarioController {
	
	@Autowired
	private UsuarioService service;
	
	@Autowired
	private UsuarioService2 service2;

	//abrir o cadastro de usuario (medico/admin/enfermeira)
	@GetMapping("/novo/cadastro/usuario")
	public String cadastroPorAdminParaAdminMedicoEnfermeira(Usuario usuario) {
		return "usuario/cadastro";
	}
	
	//salvar cadastro de usuarios por administrador
	@PostMapping("/cadastro/salvar")
	public String salvarUsuarios(Usuario usuario, RedirectAttributes attr) {
		List<Perfil> perfis = usuario.getPerfis();
		if(perfis.size() > 2 ||
				perfis.containsAll(Arrays.asList(new Perfil(1L), new Perfil(3L))) ||
				perfis.containsAll(Arrays.asList(new Perfil(2L), new Perfil(3L)))) {
			attr.addFlashAttribute("fail", "Paciente não pode ser Admin e/ou Médico.");
			attr.addFlashAttribute("usuario", usuario);
		}else {
			try {
				service.salvarUsuario(usuario);
				attr.addFlashAttribute("success", "Operação realizada com sucesso!");
			}catch (DataIntegrityViolationException ex) {
				attr.addFlashAttribute("fail", "Cadastro não realizado, email já existente.");
			}
		}
		return "redirect:/u/novo/cadastro/usuario";
	}
	
	//Abrir lista de usuários
	@GetMapping("/lista")
	public String listarUsuarios() {
		return "/usuario/lista";
	}
	
	
	//listar usuários na datatables
//	@GetMapping("/datatables/server/usuarios")
//	public ResponseEntity<?> listarUsuariosDatatables(HttpServletRequest request) {
//		System.out.println(request);
//		return ResponseEntity.ok(service.buscarTodos(request));
//	}
	
	//pre edicao de credenciais de usuarios
	//esse método não edita 
	//esse método vai pegar o id da requisição do botao de edição 
	@GetMapping("/editar/credenciais/usuario/{id}")
	public ModelAndView preEditarCredenciais(@PathVariable("id") Long id) {
		return new ModelAndView("usuario/cadastro", "usuario", service.buscarPorId(id));
	}
}
