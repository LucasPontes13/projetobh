package br.com.projetobh.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.projetobh.model.IdadeGestacional;
import br.com.projetobh.model.Paciente;
import br.com.projetobh.service.IdadeGestacionalService;
import br.com.projetobh.service.PacienteService;

@Controller
@RequestMapping("/idades")
public class IdadeGestacionalController {
	
	@Autowired
	private IdadeGestacionalService service;
	
	@Autowired
	private PacienteService pacienteService;
	
	@GetMapping("/cadastrar")
	public String cadastrar(IdadeGestacional idadeGestacional) {
		return "idadeGestacional/cadastro";
	}
	
	@PostMapping("/salvar")
	public String salvar(IdadeGestacional idadeGestacional, RedirectAttributes attr) {
		service.salvar(idadeGestacional);
		attr.addFlashAttribute("success", "Idade gestacional inserida com sucesso.");
		return "redirect:/idades/cadastrar";
	}
	
	@ModelAttribute("pacientes")
	public List<Paciente> listaPacientes(){
		return pacienteService.buscarTodos();
	}
}
