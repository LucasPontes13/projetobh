package br.com.projetobh.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.projetobh.model.Funcionario;
import br.com.projetobh.model.Medicamento;
import br.com.projetobh.service.MedicamentoService;

@Controller
@RequestMapping("/medicamentos")
public class MedicamentoController {
	
	@Autowired
	private MedicamentoService service;
		
	@GetMapping("/cadastrar")
	public String cadastrar(Medicamento medicamento) {
		return "/medicamento/cadastro";//pagina html
	}
	
	@GetMapping("/listar")
	public String listar(ModelMap model) {
		model.addAttribute("medicamentos", service.buscarTodos());// envia a lista de funcionarios
		return "/medicamento/lista";
	}
	
	@PostMapping("/salvar")
	public String salvar(@Valid Medicamento medicamento, BindingResult result, RedirectAttributes attr) {
		
		if(result.hasErrors()) {
			return "/medicamento/cadastro";
		}
		
		service.salvar(medicamento);
		attr.addFlashAttribute("success", "Medicamento salvo com sucesso");
		return "redirect:/medicamentos/cadastrar";
	}
	
	// Método que recebe a solicitação do botão de editar
	@GetMapping("/editar/{id}")
	public String preEditar(@PathVariable("id") Long id, ModelMap model) {
		model.addAttribute("medicamento", service.buscarPorId(id));
		return "/medicamento/cadastro";
	}

	@PostMapping("/editar")
	public String editar(@Valid Medicamento medicamento, BindingResult result, RedirectAttributes attr) {
		
		if (result.hasErrors()) {
			return "/medicamento/cadastro";// ai so realiza o retorno para a pagina
		}
		
		service.editar(medicamento);
		attr.addFlashAttribute("success", "Medicamento editado com sucesso.");
		return "redirect:/medicamentos/cadastrar";
	}
	
	@GetMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attr) {
		service.excluir(id);
		attr.addFlashAttribute("success", "Medicamento removido com sucesso.");
		return "redirect:/medicamentos/listar";
	}
}
