package br.com.projetobh.controller;

import br.com.projetobh.dto.request.BalancoHidricoRequest;
import br.com.projetobh.model.Paciente;
import br.com.projetobh.service.BalancoHidricoService;
import br.com.projetobh.service.PacienteService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

@Controller
@RequestMapping("/api/v1/balancos")
public class BalancoHidricoController {

	static Logger logger = Logger.getLogger("BhController.class");

	private BalancoHidricoService service;
	private PacienteService pacienteService;

	public BalancoHidricoController(BalancoHidricoService service, PacienteService pacienteService) {
		this.service = service;
		this.pacienteService = pacienteService;
	}

	@PostMapping("/salvar")
	public ResponseEntity<?> salvar(@RequestBody BalancoHidricoRequest request) {
		if(request != null){
			service.salvar(request);
			logger.info("Deu boa no cadastro de balanco hídrico.");
			return ResponseEntity.ok().build();
		}
		logger.info("BadRequest para cadastro de balanco hídrico.");
		logger.setLevel(Level.WARNING);
			return ResponseEntity.badRequest().build();
	}
	
	@GetMapping("/pacientes")
	public List<Paciente> listaPacientes(){
		return pacienteService.buscarTodos();
	}
}
