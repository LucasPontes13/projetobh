package br.com.projetobh.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.projetobh.model.Cargo;
import br.com.projetobh.model.Medicamento;
import br.com.projetobh.model.Paciente;
import br.com.projetobh.model.PrescricaoReceituario;
import br.com.projetobh.service.MedicamentoService;
import br.com.projetobh.service.PacienteService;
import br.com.projetobh.service.PrescricaoService;

@Controller
@RequestMapping("/prescricoes")
public class PrescricaoController {
	
	@Autowired
	private PrescricaoService service;
	
	@Autowired
	private PacienteService pacienteService;
	
	@Autowired
	private MedicamentoService medicamentoService;
	
	@GetMapping("/cadastrar")
	public String cadastrar(PrescricaoReceituario prescricao) {
		return "/receituario/cadastro";
	}
	
	@PostMapping("/salvar")
	public String salvar(@Valid PrescricaoReceituario prescricao, BindingResult result, RedirectAttributes attr) {
		
		//se o hasErrors retornar true significa que um campo não passou no sistema de validação 
		if(result.hasErrors()) {
			return "/receituario/cadastro";
		}
		
		service.salvar(prescricao);
		attr.addFlashAttribute("success", "Prescricao salva com sucesso.");
		return "redirect:/prescricoes/cadastrar";
	}

	//Método que recebe a solicitação do botão de editar 
	@GetMapping("/editar/{id}")
	public String preEditar(@PathVariable("id") Long id, ModelMap model) {
		model.addAttribute("prescricaoReceituario", service.buscarPorId(id));
		return "/receituario/cadastro";//esse retorno e para abrir a ppagina de cadastro
	}
	
	@PostMapping("/editar")
	public String editar(@Valid PrescricaoReceituario prescricao, BindingResult result, RedirectAttributes attr) {
		
		if(result.hasErrors()) {
			return "/receituario/cadastro";
		}
		
		service.editar(prescricao);//será passado o cargo por parametro para enviar para a dao poder editar
		attr.addFlashAttribute("success", "Prescricao editada com sucesso.");//vai enviar a mensagem para a página 
		return "redirect:/prescricoes/cadastrar";//vai fazer uma nova solicitação para a página de cadastro ser aberta novamente com o formulario limpo
	}
	
	
	@GetMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attr) {
			service.excluir(id);
			attr.addFlashAttribute("success", "Prescricao excluída com sucesso.");
		return "redirect:/prescricoes/listar";
	}

	
	@GetMapping("/listar")
	public String listar(ModelMap model) {
		model.addAttribute("prescricoes", service.buscarTodos());
		return "/receituario/lista";
	}
	
	@ModelAttribute("pacientes")
	public List<Paciente> listaPacientes(){
		return pacienteService.buscarTodos();
	}
	
	@ModelAttribute("medicamentos")
	public List<Medicamento> listaMedicamentos(){
		return medicamentoService.buscarTodos();
	}
}
