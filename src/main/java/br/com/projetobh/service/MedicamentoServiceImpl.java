package br.com.projetobh.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projetobh.dao.MedicamentoDao;
import br.com.projetobh.model.Medicamento;

@Service
@Transactional(readOnly = false)
public class MedicamentoServiceImpl implements MedicamentoService{
	
	@Autowired
	private MedicamentoDao dao;

	@Override
	public void salvar(Medicamento medicamento) {
		dao.save(medicamento);
	}

	@Override
	public void editar(Medicamento medicamento) {
		dao.update(medicamento);
	}

	@Override
	public void excluir(Long id) {
		dao.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Medicamento buscarPorId(Long id) {
		return dao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Medicamento> buscarTodos() {
		return dao.findAll();
	}

}
