package br.com.projetobh.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import br.com.projetobh.model.LiquidoEliminado;
import br.com.projetobh.model.SomaTotalDia;
import br.com.projetobh.model.SomaTotalLEliminado;
import br.com.projetobh.model.SomatorioEli;

public interface LeService {
	void salvar(LiquidoEliminado liquidoEliminado);

	void editar(LiquidoEliminado liquidoEliminado);

	void excluir(Long id);

	LiquidoEliminado buscarPorId(Long id);

	List<LiquidoEliminado> buscarTodos();
	
	List<LiquidoEliminado> buscarPorDatas(LocalDate entrada);

	List<LiquidoEliminado> buscarPorHoras(LocalTime hora);
	
	SomatorioEli parcial(LocalTime horaInicio, LocalTime horaFim, LocalDate dataCadastro);
	
	SomaTotalLEliminado totalParcialLeliminado(LocalTime hora, LocalDate dataCadastro);
	
	SomaTotalDia totalDoDia(LocalDate dataCadastro);

	Double total();
}
