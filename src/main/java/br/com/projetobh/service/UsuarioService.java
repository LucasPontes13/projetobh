package br.com.projetobh.service;

import br.com.projetobh.dao.UsuarioDao;
import br.com.projetobh.model.Perfil;
import br.com.projetobh.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UsuarioService implements UserDetailsService{
	
	@Autowired
	private UsuarioDao dao;
	
//	@Autowired
//	private Datatables datatables;
	
	/**
	 * Método que ira acessar o findByEmail lá no dao
	 */
	@Transactional(readOnly = true)
	public Usuario buscarPorEmail(String email) {
		return dao.findByEmail(email);
	}

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = buscarPorEmail(username);
		return new User(
				usuario.getEmail(),
				usuario.getSenha(),
				AuthorityUtils.createAuthorityList(getAuthorities(usuario.getPerfis()))
				);
	}
	
	private String[] getAuthorities(List<Perfil> perfis) {
		String[] authorities = new String[perfis.size()];
		for (int i = 0; i < authorities.length; i++) {
			authorities[i] = perfis.get(i).getDesc();
		}
		return authorities;
	}

	@Transactional(readOnly = false)
	public void salvarUsuario(Usuario usuario) {
		String crypt = new BCryptPasswordEncoder().encode(usuario.getSenha());
		usuario.setSenha(crypt);
		dao.saveAndFlush(usuario);
		
	}

	
//	@Transactional(readOnly = true)
//	public Map<String, Object> buscarTodos(HttpServletRequest request) {
//		datatables.setRequest(request);
//		datatables.setColunas(DatatablesColunas.USUARIOS);
//		Page<Usuario> page = datatables.getSearch().isEmpty()
//				? dao.findAll(datatables.getPageable())
//				: dao.findByEmailOrPerfil(datatables.getSearch(), datatables.getPageable());
//		return datatables.getResponse(page);
//	}

	@Transactional(readOnly = true)
	public Usuario buscarPorId(Long id) {
		return dao.findById(id).get();
	}
}	
