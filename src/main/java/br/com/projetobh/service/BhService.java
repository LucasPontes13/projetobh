package br.com.projetobh.service;

import java.util.List;

import br.com.projetobh.dto.request.BalancoHidricoRequest;
import br.com.projetobh.model.BalancoHidrico;

public interface BhService {
	void salvar(BalancoHidricoRequest request);

	void editar(BalancoHidrico balancoHidrico);

	void excluir(Long id);

	BalancoHidrico buscarPorId(Long id);

	List<BalancoHidrico> buscarTodos();
}
