package br.com.projetobh.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projetobh.dao.UsuarioDao2;
import br.com.projetobh.model.Usuario;

@Service
@Transactional(readOnly = false)
public class UsuarioServiceImpl implements UsuarioService2 {

	@Autowired
	private UsuarioDao2 dao;
	
	@Override
	public void editar(Usuario usuario) {
		dao.update(usuario);

	}

	@Override
	public void excluir(Long id) {
		dao.delete(id);
	}

	@Override
	public Usuario buscarPorId(Long id) {
		return dao.findById(id);
	}

	@Override
	public List<Usuario> buscarTodos() {
		return dao.findAll();
	}

}
