package br.com.projetobh.service;

import java.util.List;

import br.com.projetobh.model.Paciente;

public interface PacienteService {
	
	void salvar(Paciente paciente);
	
	void editar(Paciente paciente);
	
	void excluir(Long id);
	
	Paciente buscarPorId(Long id);
	
	List<Paciente> buscarTodos();
	
	boolean pacienteTemBalanco(Long id);

}
