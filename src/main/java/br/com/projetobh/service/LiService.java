package br.com.projetobh.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import br.com.projetobh.model.LiquidoInfundido;
import br.com.projetobh.model.SomaTotalDiaInfundido;
import br.com.projetobh.model.SomaTotalLInfundido;
import br.com.projetobh.model.SomatorioInf;

public interface LiService {
	void salvar(LiquidoInfundido liquidoInfundido);

	void editar(LiquidoInfundido liquidoInfundido);

	void excluir(Long id);

	LiquidoInfundido buscarPorId(Long id);

	List<LiquidoInfundido> buscarTodos();

	List<LiquidoInfundido> buscarPorDatas(LocalDate entrada);

	List<LiquidoInfundido> buscarPorHoras(LocalTime hora);
	
	List<LiquidoInfundido> buscarSm();

	SomatorioInf parcial(LocalTime horaInicio, LocalTime horaFim, LocalDate dataCadastro);
	
	SomaTotalLInfundido totalParcialLinfundido(LocalTime hora, LocalDate dataCadastro);
	
	SomaTotalDiaInfundido totalDiaInfundido(LocalDate dataCadastro);
}
