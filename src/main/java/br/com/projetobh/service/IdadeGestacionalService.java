package br.com.projetobh.service;

import java.util.List;

import br.com.projetobh.model.IdadeGestacional;

public interface IdadeGestacionalService {
	void salvar(IdadeGestacional idadeGestacional);
	void editar(IdadeGestacional idadeGestacional);
	void excluir(Long id);
	IdadeGestacional buscarPorId(Long id);
	List<IdadeGestacional> buscarTodos();
}
