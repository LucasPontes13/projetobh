package br.com.projetobh.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projetobh.dao.PacienteDao;
import br.com.projetobh.model.Paciente;
/**
 * Bean gerenciavel pelo spring
 * Se durante o projeto precisar inserir qualquer outro método em PacienteServiceImpl
 * você cria o método na interface PacienteService e implementa o método desta classe
 * @author Harmfull
 *
 */
@Service
@Transactional(readOnly = false)
public class PacienteServiceImpl implements PacienteService{

	//permite o acesso ao dao pela injeção de dependencia da interface PacienteDao
	@Autowired
	private PacienteDao dao;
	
	@Override
	public void salvar(Paciente paciente) {
		dao.save(paciente);
	}

	@Override
	public void editar(Paciente paciente) {
		dao.update(paciente);
	}

	@Override
	public void excluir(Long id) {
		dao.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Paciente buscarPorId(Long id) {
		return dao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Paciente> buscarTodos() {
		return dao.findAll();
	}

	@Override
	public boolean pacienteTemBalanco(Long id) {
		if(buscarPorId(id).getBalancoHidrico().isEmpty()) {
		return false;
		}
		
		return true;
	}

}
