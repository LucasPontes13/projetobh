package br.com.projetobh.service;

import java.time.LocalDate;
import java.time.LocalTime;

import br.com.projetobh.model.ParcialDoTurno;

public interface ParcialDoTurnoService {
	ParcialDoTurno somaParcialTurnos(LocalTime hora, LocalDate dataCadastro);
}
