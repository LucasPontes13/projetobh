package br.com.projetobh.service;

import java.util.List;

import br.com.projetobh.model.Usuario;

public interface UsuarioService2 {
	void editar(Usuario usuario);

	void excluir(Long id);

	Usuario buscarPorId(Long id);

	List<Usuario> buscarTodos();
}
