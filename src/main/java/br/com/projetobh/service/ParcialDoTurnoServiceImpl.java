package br.com.projetobh.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projetobh.dao.ParcialDao;
import br.com.projetobh.model.ParcialDoTurno;
import br.com.projetobh.model.SomaTotalLEliminado;

@Service
@Transactional(readOnly = false)
public class ParcialDoTurnoServiceImpl implements ParcialDoTurnoService{

	@Autowired
	private ParcialDao dao;
	
	
	@Override
	public ParcialDoTurno somaParcialTurnos(LocalTime hora, LocalDate dataCadastro){
		if(dataCadastro == null){
			dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		}

		return dao.somaParcialDoTurno(hora, dataCadastro);
		
	}
	
	@Scheduled(cron = "0 50 23 * * *")
	public void realizarSomatorioParcialDoTurno(){
		//LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		LocalDate dataCadastro = LocalDate.of(2019,10,27);
		ParcialDoTurno somatorio = this.somaParcialTurnos(LocalTime.of(23,47), dataCadastro);
		System.out.println(somatorio.toString());
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		//TODO: Implementar do DAO para salvar e criar a tabela
		dao.save(somatorio);
	}
	
}
