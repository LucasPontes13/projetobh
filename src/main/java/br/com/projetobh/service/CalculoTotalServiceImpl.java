package br.com.projetobh.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projetobh.dao.CalculoTotalDao;
import br.com.projetobh.model.CalculoTotal;
@Service
@Transactional(readOnly = false)
public class CalculoTotalServiceImpl implements CalculoTotalService {
	
	@Autowired
	private CalculoTotalDao dao;

	@Override
	public void salvar(CalculoTotal calculoTotal) {
		dao.save(calculoTotal);
	}

	@Transactional(readOnly = true)
	@Override
	public List<CalculoTotal> buscarTodos() {
		return dao.findAll();
	}

}
