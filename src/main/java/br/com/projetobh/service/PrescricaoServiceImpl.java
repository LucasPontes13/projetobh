package br.com.projetobh.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projetobh.dao.PrescricaoDao;
import br.com.projetobh.model.PrescricaoReceituario;

@Service
@Transactional(readOnly = false)
public class PrescricaoServiceImpl implements PrescricaoService {
	
	@Autowired
	private PrescricaoDao dao;

	@Override
	public void salvar(PrescricaoReceituario prescricao) {
		dao.save(prescricao);
	}

	@Override
	public void editar(PrescricaoReceituario prescricao) {
		dao.update(prescricao);
	}

	@Override
	public void excluir(Long id) {
		dao.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public PrescricaoReceituario buscarPorId(Long id) {
		return dao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<PrescricaoReceituario> buscarTodos() {
		return dao.findAll();
	}

	/**
	 * nao deu para implementar porque o paciente não e uma lista
	 */
//	@Override
//	public boolean prescricaoTemPaciente(Long id) {
//		
//		if(buscarPorId(id).getPaciente().isEmpty()) {
//			return false;
//		}
//		return true;
//	}
	
	

}
