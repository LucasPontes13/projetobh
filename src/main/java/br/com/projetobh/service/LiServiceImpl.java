package br.com.projetobh.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projetobh.dao.LiDao;
import br.com.projetobh.dao.SomaTotalDiaInfundidoDao;
import br.com.projetobh.dao.SomaTotalLInfundidoDao;
import br.com.projetobh.dao.SomatorioInfDao;
import br.com.projetobh.model.LiquidoInfundido;
import br.com.projetobh.model.SomaTotalDia;
import br.com.projetobh.model.SomaTotalDiaInfundido;
import br.com.projetobh.model.SomaTotalLInfundido;
import br.com.projetobh.model.SomatorioInf;

@Service
@Transactional(readOnly = false)
public class LiServiceImpl implements LiService {

	@Autowired
	private LiDao dao;
	
	@Autowired
	private SomatorioInfDao daoSomatorioInf;
	
	@Autowired
	private SomaTotalLInfundidoDao daoSomaTotal;
	
	@Autowired
	private SomaTotalDiaInfundidoDao diaInfundidoDao;

	LiquidoInfundido li = new LiquidoInfundido();

	/**
	 * MÉTODO PARA SALVAR A PARTIR DE UM HORÁRIO ESPECIFICO Irei verificar a hora
	 * atual do sistema Comparar com o horario configurado LocalTime.of(); Salvar na
	 * tabela tbl_linfundido_manha, tarde ou noite
	 */
	@Override
	public void salvar(LiquidoInfundido liquidoInfundido) {
//		//1° pegar a hora atual 
		LocalTime atual = LocalTime.now();
//		//Especificar a hora que é para salvar
		LocalTime horaCerta = LocalTime.of(00, 00);
//		//comparar as duas horas, se iguais salvar no banco 
////		if(atual != horaCerta) {
////			System.out.println("não e hora de salvar");			
////		}else {
////			liquidoInfundido.setDataCadastro(LocalDate.now());
////			liquidoInfundido.setHoraCadastro(LocalTime.now());		
////			dao.save(liquidoInfundido);
////		}
//		
		int horario = atual.compareTo(horaCerta);

		if (horario > 0) {
			liquidoInfundido.setDataCadastro(LocalDate.now());
			liquidoInfundido.setHoraCadastro(LocalTime.now());
			//parcial();
			dao.save(liquidoInfundido);
		} else if (horario == 0) {
			System.out.println("não e hora de salvar");
			// precisa retornar uma mensagem para o usuario que não é horario de calcular ou
			// que está se aproximando do hroario para calcular o parical do turno
			// essa verificação será chamada altomática no horario cadastrado e irá calcular
			// o parcial de cada turno.
			// organizar esse método de salvar novamente e montar o método para calcular e
			// verificar o horario
		}
//		
//		//liquidoInfundido.setHoraCadastro(LocalTime.now());
//		//da para utilizar p switch case 
//		
//		//switch(liquidoInfundido) {
//		
////		if(time.of(21, 45)) {
////			dao.save(liquidoInfundido);
////		}

		// liquidoInfundido.setDataCadastro(LocalDate.now());
		// liquidoInfundido.setHoraCadastro(LocalTime.now());
		// dao.save(liquidoInfundido);
	}

	@Override
	public void editar(LiquidoInfundido liquidoInfundido) {
		dao.update(liquidoInfundido);
	}

	@Override
	public void excluir(Long id) {
		dao.delete(id);
	}

	@Transactional(readOnly = true)
	@Override
	public LiquidoInfundido buscarPorId(Long id) {
		return dao.findById(id);
	}

	@Transactional(readOnly = true)
	@Override
	public List<LiquidoInfundido> buscarTodos() {
		return dao.findAll();
	}

	// método para buscar por data
	@Override
	public List<LiquidoInfundido> buscarPorDatas(LocalDate entrada) {
		if (entrada != null) {
			return dao.findByData(entrada);
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public List<LiquidoInfundido> buscarPorHoras(LocalTime hora) {
		if (hora != null) {
			return dao.findByHoras(hora);
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public List<LiquidoInfundido> buscarSm() {
		return dao.findBySm();
	}

	/**
	 * fazer um controle por um tregger aqui para somar o valor total do sm e salvar
	 * em outra tabela
	 * 
	 * - SALVAR A SOMA NA TABELA tbl_linfundido coluna 'parcial' como? - QUEM VAI
	 * CHAMAR ESSE MÉTODO ????? - EXECUTAR POR UM HORARIO???? - Não recebe
	 * parametros, irá executar a partir do horario
	 *
	 * RESOLVIDO : usando o cron ! 24/10/2019
	 */
	@Override
	public SomatorioInf parcial(LocalTime horaInicio, LocalTime horaFim, LocalDate dataCadastro) {
//		List<LiquidoInfundido> lista = new ArrayList<>();//criar um ArrayList
////		lista = (List<LiquidoInfundido>) dao.findBySm();// buscar os valores no DB e fazer cast de List para ArrayList
////		return li.getSomaParcial(lista);//
//
//		Double soma = 0.0;
//
//		for (int i = 0; i < buscarSm().size(); i++) {
//			soma += buscarSm().get(i).getSm();
//		}
//		lista.add(li);
//		return lista;
		if(dataCadastro == null){
			dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		}

		return dao.somatorioLiquidoInfundido(horaInicio, horaFim, dataCadastro);
	}

	/**
	 * Jobs que iram rodar de forma automatica com base no cron passado, o qual diz quando e em qual horário
	 * deve rodar .
	 *
	 */
	@Scheduled(cron = "5 40 20 * * *")
	public void realizarSomatorioTurnoNoite(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		SomatorioInf somatorio = this.parcial(LocalTime.of(19,50,0), LocalTime.of(19,55,0), dataCadastro);
		System.out.println(somatorio.toString());
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		daoSomatorioInf.save(somatorio);
	}
	@Scheduled(cron = "7 40 20 * * *")
	public void realizarSomatorioTurnoMadrugada(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		SomatorioInf somatorio = this.parcial(LocalTime.of(19,56), LocalTime.of(19,58),dataCadastro);
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		daoSomatorioInf.save(somatorio);
	}
	@Scheduled(cron = "10 40 20 * * *")
	public void realizarSomatorioTurnoManha(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		SomatorioInf somatorio = this.parcial(LocalTime.of(19,59), LocalTime.of(20,0),dataCadastro);
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		daoSomatorioInf.save(somatorio);
	}
	@Scheduled(cron = "12 40 20 * * *")
	public void realizarSomatorioTurnoTarde(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		SomatorioInf somatorio = this.parcial(LocalTime.of(20,05), LocalTime.of(20,10),dataCadastro);
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		//TODO: Implementar do DAO para salvar e criar a tabela
		daoSomatorioInf.save(somatorio);
	}
/*****************************************************************************************************/	
	@Override
	public SomaTotalLInfundido totalParcialLinfundido(LocalTime hora, LocalDate dataCadastro){
		if(dataCadastro == null){
			dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		}

		return dao.somaTotalLInfundido(hora, dataCadastro);
		
	}
	
	/**
	 * Job que irá rodar de forma automatica com base no cron passado, o qual diz quando e em qual horário
	 * deve rodar  para realizar a soma do total da cada liquido infundido.
	 *
	 */
	@Scheduled(cron = "2 48 20 * * *")
	public void realizarSomaTotalTurnoNoite(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		//LocalDate dataCadastro = LocalDate.of(2019,10,27);
		SomaTotalLInfundido somatorio = this.totalParcialLinfundido(LocalTime.of(20,40,5), dataCadastro);
		System.out.println(somatorio.toString());
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		//TODO: Implementar do DAO para salvar e criar a tabela
		daoSomaTotal.save(somatorio);
	}
	
	@Scheduled(cron = "5 48 20 * * *")
	public void realizarSomaTotalTurnoMadrugada(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		//LocalDate dataCadastro = LocalDate.of(2019,10,27);
		SomaTotalLInfundido somatorio = this.totalParcialLinfundido(LocalTime.of(20,40,7), dataCadastro);
		System.out.println(somatorio.toString());
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		//TODO: Implementar do DAO para salvar e criar a tabela
		daoSomaTotal.save(somatorio);
	}
	
	@Scheduled(cron = "7 48 20 * * *")
	public void realizarSomaTotalTurnoManha(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		//LocalDate dataCadastro = LocalDate.of(2019,10,27);
		SomaTotalLInfundido somatorio = this.totalParcialLinfundido(LocalTime.of(20,40,10), dataCadastro);
		System.out.println(somatorio.toString());
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		//TODO: Implementar do DAO para salvar e criar a tabela
		daoSomaTotal.save(somatorio);
	}
	
	@Scheduled(cron = "9 48 20 * * *")
	public void realizarSomaTotalTurnoTarde(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		//LocalDate dataCadastro = LocalDate.of(2019,10,27);
		SomaTotalLInfundido somatorio = this.totalParcialLinfundido(LocalTime.of(20,40,12), dataCadastro);
		System.out.println(somatorio.toString());
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		//TODO: Implementar do DAO para salvar e criar a tabela
		daoSomaTotal.save(somatorio);
	}
	
	/********************************************************************************************************************/
	/**
	 * Espaço utilizado para o método que ira chamar a consulta no banco de dados a partir de um unico horário do dia
	 * para o calculo total do liquido em 24h
	 */
	
	
	@Override
	public SomaTotalDiaInfundido totalDiaInfundido(LocalDate dataCadastro){
		if(dataCadastro == null){
			dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		}

		return dao.somaTotalDiaInfundido(dataCadastro);
		
	}
	
	@Scheduled(cron = "25 5 15 * * *")
	public void realizarSomaTotalDoDiaInfundido(){
		//LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		LocalDate dataCadastro = LocalDate.of(2019,11,1);
		SomaTotalDiaInfundido somatorio = this.totalDiaInfundido(dataCadastro);
		System.out.println(somatorio.toString());
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		//TODO: Implementar do DAO para salvar e criar a tabela
		diaInfundidoDao.save(somatorio);
	}

}
