package br.com.projetobh.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projetobh.dao.LeDao;
import br.com.projetobh.dao.SomaTotalDiaDao;
import br.com.projetobh.dao.SomaTotalLEliminadoDao;
import br.com.projetobh.dao.SomatorioEliDao;
import br.com.projetobh.model.LiquidoEliminado;
import br.com.projetobh.model.SomaTotalDia;
import br.com.projetobh.model.SomaTotalLEliminado;
import br.com.projetobh.model.SomatorioEli;

@Service
@Transactional(readOnly = false)
public class LeServiceImpl implements LeService{
	
	@Autowired
	private LeDao dao;
	
	@Autowired
	private SomatorioEliDao daoSomatorioEli;
	
	@Autowired
	private SomaTotalLEliminadoDao daoSomaTotal;
	
	@Autowired
	private SomaTotalDiaDao totalDiaDao;

	/**
	 * MÉTODO PARA SALVAR A PARTIR DE UM HORÁRIO ESPECIFICO
	 * Irei verificar a hora atual do sistema
	 * Comparar com o horario configurado LocalTime.of();
	 * Salvar na tabela tbl_leliminado_manha, tarde ou noite
	 */
	@Override
	public void salvar(LiquidoEliminado liquidoEliminado) {
		//1° pegar a hora atual
		LocalTime atual = LocalTime.now();
		//Especificar a hora limite para salvar
		LocalTime limite = LocalTime.of(00, 26);
		
		int horario = atual.compareTo(limite);
		
		if(horario > 0) {
			liquidoEliminado.setDataCadastro(LocalDate.now());
			liquidoEliminado.setHoraCadastro(LocalTime.now());
			dao.save(liquidoEliminado);
		}else if(horario == 0) {
			System.out.println("não é hora de salvar");
//			//precisa retornar uma mensagem para o usuario que não é horario de calcular ou que está se aproximando do hroario para calcular o parical do turno
//			//essa verificação será chamada altomática no horario cadastrado e irá calcular o parcial de cada turno. 
//			//organizar esse método de salvar novamente e montar o método para calcular e verificar o horario
		}
		
		
		//liquidoEliminado.setDataCadastro(LocalDate.now());
		//dao.save(liquidoEliminado);
	}

	@Override
	public void editar(LiquidoEliminado liquidoEliminado) {
		//colocar a hora que o liquido foi editado utilizando da mesma forma no salvar
		dao.update(liquidoEliminado);
	}

	@Override
	public void excluir(Long id) {
		dao.delete(id);
	}
	
	@Transactional(readOnly = true)
	@Override
	public LiquidoEliminado buscarPorId(Long id) {
		return dao.findById(id);
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<LiquidoEliminado> buscarTodos() {
		return dao.findAll();
	}

	@Override
	public List<LiquidoEliminado> buscarPorDatas(LocalDate entrada) {
		if(entrada != null) {
			return dao.findByData(entrada);
		}else {
			return new ArrayList<>();
		}
	}

	@Override
	public List<LiquidoEliminado> buscarPorHoras(LocalTime hora) {
		if(hora != null) {
			return dao.findByHoras(hora);
		}else {
			return new ArrayList<>();
		}
		
	}

	@Override
	public Double total() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public SomatorioEli parcial(LocalTime horaInicio, LocalTime horaFim, LocalDate dataCadastro) {
		if(dataCadastro == null){
			dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		}

		return dao.somatorioLiquidoEliminado(horaInicio, horaFim, dataCadastro);
	}
	
	/**
	 * Jobs que iram rodar de forma automatica com base no cron passado, o qual diz quando e em qual horário
	 * deve rodar .
	 *
	 */
	@Scheduled(cron = "5 40 20 * * *")
	public void realizarSomatorioTurnoNoite(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		SomatorioEli somatorio = this.parcial(LocalTime.of(19,50,0), LocalTime.of(19,55,0), dataCadastro);
		System.out.println(somatorio.toString());
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		daoSomatorioEli.save(somatorio);
	}
	@Scheduled(cron = "7 40 20 * * *")
	public void realizarSomatorioTurnoMadrugada(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		SomatorioEli somatorio = this.parcial(LocalTime.of(19,55), LocalTime.of(19,58),dataCadastro);
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		daoSomatorioEli.save(somatorio);
	}
	@Scheduled(cron = "10 40 20 * * *")
	public void realizarSomatorioTurnoManha(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		SomatorioEli somatorio = this.parcial(LocalTime.of(20,0), LocalTime.of(20,04),dataCadastro);
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		daoSomatorioEli.save(somatorio);
	}
	@Scheduled(cron = "12 40 20 * * *")
	public void realizarSomatorioTurnoTarde(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		SomatorioEli somatorio = this.parcial(LocalTime.of(20,05), LocalTime.of(20,10),dataCadastro);
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		//TODO: Implementar do DAO para salvar e criar a tabela
		daoSomatorioEli.save(somatorio);
	}
	
	/*****************************************************************************************************/	
	@Override
	public SomaTotalLEliminado totalParcialLeliminado(LocalTime hora, LocalDate dataCadastro){
		if(dataCadastro == null){
			dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		}

		return dao.somaTotalLEliminado(hora, dataCadastro);
		
	}
	
	/**
	 * Job que irá rodar de forma automatica com base no cron passado, o qual diz quando e em qual horário
	 * deve rodar  para realizar a soma do total da cada liquido infundido.
	 *
	 */
	@Scheduled(cron = "2 48 20 * * *")
	public void realizarSomaTotalTurnoNoite(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		//LocalDate dataCadastro = LocalDate.of(2019,10,27);
		SomaTotalLEliminado somatorio = this.totalParcialLeliminado(LocalTime.of(20,40,5), dataCadastro);
		System.out.println(somatorio.toString());
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		//TODO: Implementar do DAO para salvar e criar a tabela
		daoSomaTotal.save(somatorio);
	}
	
	@Scheduled(cron = "5 48 20 * * *")
	public void realizarSomaTotalTurnoMadrugada(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		//LocalDate dataCadastro = LocalDate.of(2019,10,27);
		SomaTotalLEliminado somatorio = this.totalParcialLeliminado(LocalTime.of(20,40,7), dataCadastro);
		System.out.println(somatorio.toString());
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		//TODO: Implementar do DAO para salvar e criar a tabela
		daoSomaTotal.save(somatorio);
	}
	
	@Scheduled(cron = "7 48 20 * * *")
	public void realizarSomaTotalTurnoManha(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		//LocalDate dataCadastro = LocalDate.of(2019,10,27);
		SomaTotalLEliminado somatorio = this.totalParcialLeliminado(LocalTime.of(20,40,10), dataCadastro);
		System.out.println(somatorio.toString());
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		//TODO: Implementar do DAO para salvar e criar a tabela
		daoSomaTotal.save(somatorio);
	}
	
	@Scheduled(cron = "9 48 20 * * *")
	public void realizarSomaTotalTurnoTarde(){
		LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		//LocalDate dataCadastro = LocalDate.of(2019,10,27);
		SomaTotalLEliminado somatorio = this.totalParcialLeliminado(LocalTime.of(20,40,12), dataCadastro);
		System.out.println(somatorio.toString());
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		//TODO: Implementar do DAO para salvar e criar a tabela
		daoSomaTotal.save(somatorio);
	}
	
	
	/********************************************************************************************************************/
	/**
	 * Espaço utilizado para o método que ira chamar a consulta no banco de dados a partir de um unico horário do dia
	 * para o calculo total do liquido em 24h
	 */
	
	
	@Override
	public SomaTotalDia totalDoDia(LocalDate dataCadastro){
		if(dataCadastro == null){
			dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		}

		return dao.somaTotalDia(dataCadastro);
		
	}
	
	@Scheduled(cron = "25 5 15 * * *")
	public void realizarSomaTotalDoDia(){
		//LocalDate dataCadastro = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		LocalDate dataCadastro = LocalDate.of(2019,11,1);
		SomaTotalDia somatorio = this.totalDoDia(dataCadastro);
		System.out.println(somatorio.toString());
		somatorio.setDataCadastro(LocalDate.now(ZoneId.of("America/Sao_Paulo")));
		somatorio.setHoraCadastro(LocalTime.now(ZoneId.of("America/Sao_Paulo")));
		//TODO: Implementar do DAO para salvar e criar a tabela
		totalDiaDao.save(somatorio);
	}
	
}
