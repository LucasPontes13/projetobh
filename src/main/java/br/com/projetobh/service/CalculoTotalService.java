package br.com.projetobh.service;

import java.util.List;

import br.com.projetobh.model.CalculoTotal;

public interface CalculoTotalService {
	
	void salvar(CalculoTotal calculoTotal);
	
	List<CalculoTotal> buscarTodos();
}
