package br.com.projetobh.service;

import java.util.List;

import br.com.projetobh.model.Medicamento;

public interface MedicamentoService {
	void salvar(Medicamento medicamento);
	void editar(Medicamento medicamento);
	void excluir(Long id);
	Medicamento buscarPorId(Long id);
	List<Medicamento> buscarTodos();
}
