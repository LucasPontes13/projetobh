package br.com.projetobh.service;

import java.util.List;

import br.com.projetobh.model.PrescricaoReceituario;

public interface PrescricaoService {
	void salvar(PrescricaoReceituario prescricao);

	void editar(PrescricaoReceituario prescricao);

	void excluir(Long id);

	PrescricaoReceituario buscarPorId(Long id);

	List<PrescricaoReceituario> buscarTodos();
	
	//boolean prescricaoTemPaciente(Long id);
}
