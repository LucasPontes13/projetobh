package br.com.projetobh.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import br.com.projetobh.dto.request.BalancoHidricoRequest;
import br.com.projetobh.model.Paciente;
import br.com.projetobh.repository.BalancoHidricoRepository;
import br.com.projetobh.repository.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projetobh.dao.BhDao;
import br.com.projetobh.model.BalancoHidrico;

@Service
@Transactional(readOnly = false)
public class BalancoHidricoService{

	private BalancoHidricoRepository bhRepository;
	private PacienteRepository pacienteRepository;

	public BalancoHidricoService(BalancoHidricoRepository bhRepository, PacienteRepository pacienteRepository) {
		this.bhRepository = bhRepository;
		this.pacienteRepository = pacienteRepository;
	}

	public void salvar(BalancoHidricoRequest request) {
		var model = request.toModel();
		//preciso colocar uma exception quando não tem o paciente.
		//não pode criar um balanço sem ter um paciente associado.
//		Paciente paciente = buscaPacienteExistente(model.getPaciente().getId());//aqui não tem id
//		request.setDataCadastro(LocalDate.now());
		bhRepository.save(model);
	}

	public void editar(BalancoHidrico balancoHidrico) {
//		dao.update(balancoHidrico);
	}

	public void excluir(Long id) {
//		dao.delete(id);
		
	}
	
	@Transactional(readOnly = true)
	public BalancoHidrico buscarPorId(Long id) {
//		return dao.findById(id);
		return null;
	}
	
	@Transactional(readOnly = true)
	public List<BalancoHidrico> buscarTodos() {
//		return dao.findAll();
		return null;
	}

	private Paciente buscaPacienteExistente(Long id){
		Optional<Paciente> paciente = null;
		try {
			paciente = pacienteRepository.findById(id);
			if (paciente.isEmpty()) return null;
		}catch (Exception e) {
			//lançar uma exception amigável.
			System.out.println("Deu ruim no service.");
		}
		return paciente.get();
	}

}
