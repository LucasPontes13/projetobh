package br.com.projetobh.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projetobh.dao.IdadeGestacionalDao;
import br.com.projetobh.model.IdadeGestacional;

@Service
@Transactional(readOnly = false)
public class IdadeGestacionalServiceImpl implements IdadeGestacionalService{
	
	@Autowired
	private IdadeGestacionalDao dao;

	@Override
	public void salvar(IdadeGestacional idadeGestacional) {
		dao.save(idadeGestacional);
	}

	@Override
	public void editar(IdadeGestacional idadeGestacional) {
		dao.update(idadeGestacional);
	}

	@Override
	public void excluir(Long id) {
		dao.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public IdadeGestacional buscarPorId(Long id) {
		return dao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<IdadeGestacional> buscarTodos() {
		return dao.findAll();
	}

}
