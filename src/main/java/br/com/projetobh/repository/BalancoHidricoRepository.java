package br.com.projetobh.repository;

import br.com.projetobh.model.BalancoHidrico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BalancoHidricoRepository extends JpaRepository<BalancoHidrico, Long> {
}
