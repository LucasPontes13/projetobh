package br.com.projetobh.model;

public enum PerfilTipo {
	ADMIN(1, "ADMIN"), 
	MEDICO(2, "MEDICO"), 
	ENFERMEIRA(3, "ENFERMEIRA");

	private long cod;
	private String desc;

	PerfilTipo(long cod, String desc) {
		this.cod = cod;
		this.desc = desc;
	}

	public long getCod() {
		return cod;
	}

	public void setCod(long cod) {
		this.cod = cod;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
}
