package br.com.projetobh.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "perfis")
public class Perfil extends AbstractEntity<Long> {
	
	@Column(name = "descricao", nullable = false, unique = true)
	private String desc;
	
	@Enumerated(EnumType.STRING)
	private PerfilTipo perfilTipo;
	
	public Perfil() {
		super();
	}

	public Perfil(Long id) {
		super.setId(id);
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public PerfilTipo getPerfilTipo() {
		return perfilTipo;
	}
	
	public void setPerfilTipo(PerfilTipo perfilTipo) {
		this.perfilTipo = perfilTipo;
	}
}

