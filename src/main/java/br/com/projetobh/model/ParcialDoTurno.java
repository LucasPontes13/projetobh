package br.com.projetobh.model;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@SuppressWarnings("serial")
@Entity
@Table(name = "tbl_parciaisTurnos")
public class ParcialDoTurno extends AbstractEntity<Long> {

	private Double parcial;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate dataCadastro;

	private LocalTime horaCadastro;

	public ParcialDoTurno() {

	}

	public ParcialDoTurno(Double parcial) {
		this.parcial = parcial;
	}

	public Double getParcial() {
		return parcial;
	}

	public void setParcial(Double parcial) {
		this.parcial = parcial;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public LocalTime getHoraCadastro() {
		return horaCadastro;
	}

	public void setHoraCadastro(LocalTime horaCadastro) {
		this.horaCadastro = horaCadastro;
	}

	@Override
	public String toString() {
		return "ParcialDoTurno [parcial=" + parcial + ", dataCadastro=" + dataCadastro + ", horaCadastro="
				+ horaCadastro + "]";
	}

}
