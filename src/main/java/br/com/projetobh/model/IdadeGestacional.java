package br.com.projetobh.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class IdadeGestacional extends AbstractEntity<Long> {

	private double ig;

	private int semanaGestacaoInicio;

	private int semanaGestacaoFinal;
//
//	@DateTimeFormat(iso = ISO.DATE)
//	@Column(name = "dataHoraAlteracaoRegistro", columnDefinition = "DATE")
//	private LocalDate dataHoraAlteracaoRegistro;

	//@NotNull(message = "Selecione o paciente.") montar uma div no form para o pacienteddd
	@ManyToOne
	@JoinColumn(name = "id_paciente_fk")
	private Paciente paciente;

	//construtor
	public IdadeGestacional() {}

	public double getIg() {
		return ig;
	}
	
	public void setIg(double ig) {
		this.ig = ig;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public int getSemanaGestacaoInicio() {
		return semanaGestacaoInicio;
	}

	public void setSemanaGestacaoInicio(int semanaGestacaoInicio) {
		this.semanaGestacaoInicio = semanaGestacaoInicio;
	}

	public int getSemanaGestacaoFinal() {
		return semanaGestacaoFinal;
	}

	public void setSemanaGestacaoFinal(int semanaGestacaoFinal) {
		this.semanaGestacaoFinal = semanaGestacaoFinal;
	}
	
	


}
