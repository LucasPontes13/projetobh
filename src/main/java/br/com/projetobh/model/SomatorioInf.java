package br.com.projetobh.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalTime;

//TODO: criar a entidade e a table
@Entity
@Table(name = "tbl_liqInfundidoSomatorio")
@SuppressWarnings("serial")
public class SomatorioInf extends AbstractEntity<Long>{

	private Double npt;
	private Double flush;
	private Double hemo;
	private Double sm;
	private Double sondaLmLa;
	private Double soro;
	private Double voLmLa;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
//	@Column(name = "data_cadastro", nullable = false, columnDefinition = "DATE")
	private LocalDate dataCadastro;

	private LocalTime horaCadastro;
	
	public SomatorioInf() {
		
	}

	public SomatorioInf(Double npt, Double flush, Double hemo, Double sm, Double sondaLmLa, Double soro, Double voLmLa) {
		this.npt = npt;
		this.flush = flush;
		this.hemo = hemo;
		this.sm = sm;
		this.sondaLmLa = sondaLmLa;
		this.soro = soro;
		this.voLmLa = voLmLa;
	}

	public Double getNpt() {
		return npt;
	}

	public void setNpt(Double npt) {
		this.npt = npt;
	}

	public Double getFlush() {
		return flush;
	}

	public void setFlush(Double flush) {
		this.flush = flush;
	}

	public Double getHemo() {
		return hemo;
	}

	public void setHemo(Double hemo) {
		this.hemo = hemo;
	}

	public Double getSm() {
		return sm;
	}

	public void setSm(Double sm) {
		this.sm = sm;
	}

	public Double getSondaLmLa() {
		return sondaLmLa;
	}

	public void setSondaLmLa(Double sondaLmLa) {
		this.sondaLmLa = sondaLmLa;
	}

	public Double getSoro() {
		return soro;
	}

	public void setSoro(Double soro) {
		this.soro = soro;
	}

	public Double getVoLmLa() {
		return voLmLa;
	}

	public void setVoLmLa(Double voLmLa) {
		this.voLmLa = voLmLa;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public LocalTime getHoraCadastro() {
		return horaCadastro;
	}

	public void setHoraCadastro(LocalTime horaCadastro) {
		this.horaCadastro = horaCadastro;
	}

	@Override
	public String toString() {
		return "SomatorioInf{" +
				"npt=" + npt +
				", flush=" + flush +
				", hemo=" + hemo +
				", sm=" + sm +
				", sondaLmLa=" + sondaLmLa +
				", soro=" + soro +
				", voLmLa=" + voLmLa +
				", dataCadastro=" + dataCadastro +
				", horaCadastro=" + horaCadastro +
				'}';
	}
}
