package br.com.projetobh.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@SuppressWarnings("serial")
@Entity
@Table(name = "tbl_medicamento")
public class Medicamento extends AbstractEntity<Long>{

	private String nomeComercial;
	private String nomeGenerico;
	private String laboratorio;
	private int lote;
	
	@DateTimeFormat(iso = ISO.DATE)
	@Column(name = "data_vencimento", columnDefinition = "DATE")
	private LocalDate vencimento;
	private int concentracaoMedicamento;

	@ManyToOne
	@JoinColumn(name = "id_prescricao")
	private PrescricaoReceituario prescricaoReceituario;

	public Medicamento() {

	}

	public Medicamento(String nomeComercial, String nomeGenerico, String laboratorio, int lote, LocalDate vencimento,
			int concentracaoMedicamento) {
		this.nomeComercial = nomeComercial;
		this.nomeGenerico = nomeGenerico;
		this.laboratorio = laboratorio;
		this.lote = lote;
		this.vencimento = vencimento;
		this.concentracaoMedicamento = concentracaoMedicamento;
	}

	public Medicamento(String nomeComercial, String nomeGenerico, String laboratorio, int lote,
			LocalDate vencimento, int concentracaoMedicamento, PrescricaoReceituario prescricaoReceituario) {
		this.nomeComercial = nomeComercial;
		this.nomeGenerico = nomeGenerico;
		this.laboratorio = laboratorio;
		this.lote = lote;
		this.vencimento = vencimento;
		this.concentracaoMedicamento = concentracaoMedicamento;
		this.prescricaoReceituario = prescricaoReceituario;
	}

	public String getNomeComercial() {
		return nomeComercial;
	}

	public void setNomeComercial(String nomeComercial) {
		this.nomeComercial = nomeComercial;
	}

	public String getNomeGenerico() {
		return nomeGenerico;
	}

	public void setNomeGenerico(String nomeGenerico) {
		this.nomeGenerico = nomeGenerico;
	}

	public String getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(String laboratorio) {
		this.laboratorio = laboratorio;
	}

	public int getLote() {
		return lote;
	}

	public void setLote(int lote) {
		this.lote = lote;
	}

	public LocalDate getVencimento() {
		return vencimento;
	}

	public void setVencimento(LocalDate vencimento) {
		this.vencimento = vencimento;
	}

	public int getConcentracaoMedicamento() {
		return concentracaoMedicamento;
	}

	public void setConcentracaoMedicamento(int concentracaoMedicamento) {
		this.concentracaoMedicamento = concentracaoMedicamento;
	}

	public PrescricaoReceituario getPrescricaoReceituario() {
		return prescricaoReceituario;
	}

	public void setPrescricaoReceituario(PrescricaoReceituario prescricaoReceituario) {
		this.prescricaoReceituario = prescricaoReceituario;
	}
	
}
