package br.com.projetobh.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import javax.persistence.Entity;

@SuppressWarnings("serial")
@Entity
public class CalculoTotal extends AbstractEntity<Long> {

	// private LiquidoEliminado liquidosEliminados;

	// private LiquidoInfundido liquidosInfundidos;
	
	private double calculoParcial;

	private LocalDate dataCadastro;

	private LocalTime horaCadastro;

	public CalculoTotal() {
	}

	public double getCalculoParcial() {
		return calculoParcial;
	}
	
	public void setCalculoParcial(double calculoParcial) {
		this.calculoParcial = calculoParcial;
	}
	
	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public LocalTime getHoraCadastro() {
		return horaCadastro;
	}

	public void setHoraCadastro(LocalTime horaCadastro) {
		this.horaCadastro = horaCadastro;
	}
	
	Double soma = 0.0;
	// Método para calcular o total das parciais dos três turnos
	public Double somaParcial(ArrayList<LiquidoInfundido> parcial) {
		for (int i = 0; i < parcial.size(); i++) {
			soma = soma + parcial.get(i).getSm();
		}
		return soma;
	}

}
