package br.com.projetobh.model;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@SuppressWarnings("serial")
@Entity
@Table(name = "soma_total_dia_eliminado")
public class SomaTotalDia extends AbstractEntity<Long> {

	private Double urina;
	private Double fezes;
	private Double vomito;
	private Double dreno;
	private Double estase;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate dataCadastro;

	private LocalTime horaCadastro;

	public SomaTotalDia() {

	}

	public SomaTotalDia(Double urina, Double fezes, Double vomito, Double dreno, Double estase) {
		this.urina = urina;
		this.fezes = fezes;
		this.vomito = vomito;
		this.dreno = dreno;
		this.estase = estase;
	}

	public Double getUrina() {
		return urina;
	}

	public void setUrina(Double urina) {
		this.urina = urina;
	}

	public Double getFezes() {
		return fezes;
	}

	public void setFezes(Double fezes) {
		this.fezes = fezes;
	}

	public Double getVomito() {
		return vomito;
	}

	public void setVomito(Double vomito) {
		this.vomito = vomito;
	}

	public Double getDreno() {
		return dreno;
	}

	public void setDreno(Double dreno) {
		this.dreno = dreno;
	}

	public Double getEstase() {
		return estase;
	}

	public void setEstase(Double estase) {
		this.estase = estase;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public LocalTime getHoraCadastro() {
		return horaCadastro;
	}

	public void setHoraCadastro(LocalTime horaCadastro) {
		this.horaCadastro = horaCadastro;
	}

	@Override
	public String toString() {
		return "SomaTotalDia [urina=" + urina + ", fezes=" + fezes + ", vomito=" + vomito + ", dreno=" + dreno
				+ ", estase=" + estase + "]";
	}

}
