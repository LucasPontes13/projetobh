package br.com.projetobh.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
public class PrescricaoReceituario extends AbstractEntity<Long> {

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "prescricaoReceituario")
	private List<Medicamento> medicamento;

	//@NotNull
	@ManyToOne
	@JoinColumn(name = "id_pessoa")
	private Paciente paciente;

	private String prescricao;
	
	private LocalDateTime dataHoraPrescricao;

	public PrescricaoReceituario() {

	}

	public PrescricaoReceituario(String prescricao, LocalDateTime dataHoraPrescricao) {
		this.prescricao = prescricao;
		this.dataHoraPrescricao = dataHoraPrescricao;
	}

	public PrescricaoReceituario(List<Medicamento> medicamento, Paciente paciente, String prescricao,
			LocalDateTime dataHoraPrescricao) {
		this.medicamento = medicamento;
		this.paciente = paciente;
		this.prescricao = prescricao;
		this.dataHoraPrescricao = dataHoraPrescricao;
	}

	public List<Medicamento> getMedicamento() {
		return medicamento;
	}

	public void setMedicamento(List<Medicamento> medicamento) {
		this.medicamento = medicamento;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public String getPrescricao() {
		return prescricao;
	}

	public void setPrescricao(String prescricao) {
		this.prescricao = prescricao;
	}

	public LocalDateTime getDataHoraPrescricao() {
		return dataHoraPrescricao;
	}

	public void setDataHoraPrescricao(LocalDateTime dataHoraPrescricao) {
		this.dataHoraPrescricao = dataHoraPrescricao;
	}

	@Override
	public String toString() {
		return "PrescricaoReceituario [medicamento=" + medicamento + ", paciente=" + paciente + ", prescricao="
				+ prescricao + ", dataHoraPrescricao=" + dataHoraPrescricao + ", getMedicamento()=" + getMedicamento()
				+ ", getPaciente()=" + getPaciente() + ", getPrescricao()=" + getPrescricao()
				+ ", getDataHoraPrescricao()=" + getDataHoraPrescricao() + ", getId()=" + getId() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + "]";
	}

	

}
