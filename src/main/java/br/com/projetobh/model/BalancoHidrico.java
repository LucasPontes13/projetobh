package br.com.projetobh.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "tbl_balanco_hidrico")
public class BalancoHidrico extends AbstractEntity<Long>{
	
	private String nomeBh; //será utilizado esse nome para associar ao paciente
	
	@ManyToOne(cascade = CascadeType.ALL)//para salvar o paciente quando salvar o balanço.
	@JoinColumn(name = "id_paciente_fk")
	private Paciente paciente;
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "balancoHidrico")
	private List<LiquidoInfundido> liquidoInfundido;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "balancoHidrico")
	private List<LiquidoEliminado> liquidoEliminado;
	
	private LocalDate dataCadastro;

	public BalancoHidrico(String nomeBh, Paciente paciente, List<LiquidoInfundido> liquidoInfundido,
						  List<LiquidoEliminado> liquidoEliminado, LocalDate dataCadastro) {
		this.nomeBh = nomeBh;
		this.paciente = paciente;
		this.liquidoInfundido = liquidoInfundido;
		this.liquidoEliminado = liquidoEliminado;
		this.dataCadastro = dataCadastro;
	}

	public String getNomeBh() {
		return nomeBh;
	}

	public void setNomeBh(String nomeBh) {
		this.nomeBh = nomeBh;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public List<LiquidoInfundido> getLiquidoInfundido() {
		return liquidoInfundido;
	}

	public void setLiquidoInfundido(List<LiquidoInfundido> liquidoInfundido) {
		this.liquidoInfundido = liquidoInfundido;
	}

	public List<LiquidoEliminado> getLiquidoEliminado() {
		return liquidoEliminado;
	}

	public void setLiquidoEliminado(List<LiquidoEliminado> liquidoEliminado) {
		this.liquidoEliminado = liquidoEliminado;
	}
	
	public LocalDate getDataCadastro() {
		return dataCadastro;
	}
	
	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}	
}
