package br.com.projetobh.model;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@SuppressWarnings("serial")
@Entity
@Table(name = "tbl_somaTotalLiqInfundido")
public class SomaTotalLInfundido extends AbstractEntity<Long> {

	private Double somaTotalLiquidos;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate dataCadastro;

	private LocalTime horaCadastro;

	public SomaTotalLInfundido() {
		
	}

	public SomaTotalLInfundido(Double somaTotalLiquidos) {
		this.somaTotalLiquidos = somaTotalLiquidos;
	}

	public Double getSomaTotalLiquidos() {
		return somaTotalLiquidos;
	}

	public void setSomaTotalLiquidos(Double somaTotalLiquidos) {
		this.somaTotalLiquidos = somaTotalLiquidos;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public LocalTime getHoraCadastro() {
		return horaCadastro;
	}

	public void setHoraCadastro(LocalTime horaCadastro) {
		this.horaCadastro = horaCadastro;
	}

	@Override
	public String toString() {
		return "SomaTotalLInfundido [somaTotalLiquidos=" + somaTotalLiquidos + ", dataCadastro=" + dataCadastro
				+ ", horaCadastro=" + horaCadastro + "]";
	}

}
