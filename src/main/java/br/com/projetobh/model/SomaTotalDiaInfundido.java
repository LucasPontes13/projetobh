package br.com.projetobh.model;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@SuppressWarnings("serial")
@Entity
@Table(name = "soma_total_dia_infundido")
public class SomaTotalDiaInfundido extends AbstractEntity<Long> {

	private Double sm;
	private Double voLmLa;
	private Double sondaLmLa;
	private Double soro;
	private Double npt;
	private Double hemo;
	private Double flush;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate dataCadastro;

	private LocalTime horaCadastro;

	public SomaTotalDiaInfundido() {

	}

	public SomaTotalDiaInfundido(Double sm, Double voLmLa, Double sondaLmLa, Double soro, Double npt, Double hemo,
			Double flush) {
		this.sm = sm;
		this.voLmLa = voLmLa;
		this.sondaLmLa = sondaLmLa;
		this.soro = soro;
		this.npt = npt;
		this.hemo = hemo;
		this.flush = flush;
	}

	public Double getSm() {
		return sm;
	}

	public void setSm(Double sm) {
		this.sm = sm;
	}

	public Double getVoLmLa() {
		return voLmLa;
	}

	public void setVoLmLa(Double voLmLa) {
		this.voLmLa = voLmLa;
	}

	public Double getSondaLmLa() {
		return sondaLmLa;
	}

	public void setSondaLmLa(Double sondaLmLa) {
		this.sondaLmLa = sondaLmLa;
	}

	public Double getSoro() {
		return soro;
	}

	public void setSoro(Double soro) {
		this.soro = soro;
	}

	public Double getNpt() {
		return npt;
	}

	public void setNpt(Double npt) {
		this.npt = npt;
	}

	public Double getHemo() {
		return hemo;
	}

	public void setHemo(Double hemo) {
		this.hemo = hemo;
	}

	public Double getFlush() {
		return flush;
	}

	public void setFlush(Double flush) {
		this.flush = flush;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public LocalTime getHoraCadastro() {
		return horaCadastro;
	}

	public void setHoraCadastro(LocalTime horaCadastro) {
		this.horaCadastro = horaCadastro;
	}

}
