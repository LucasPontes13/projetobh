package br.com.projetobh.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
@Table(name = "tbl_paciente")
public class Paciente extends AbstractEntity<Long> {

//	vai precisar gerar o numero do prontuário do paciente
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
//	@NotNull
//	private int prontuario;

	private String nomeCompleto;

	private String sexo;

	// @PastOrPresent(message = "{PastOrPresent.funcionario.dataEntrada}")
	@DateTimeFormat(iso = ISO.DATE)
	@Column(name = "data_nascimento", columnDefinition = "DATE")
	private LocalDate dataNascimento;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "paciente")
	private List<IdadeGestacional> idadeGestacional;

	// @PastOrPresent(message = "{PastOrPresent.funcionario.dataEntrada}")
	@DateTimeFormat(iso = ISO.DATE)
	@Column(name = "data_nascimento", columnDefinition = "DATE")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "paciente")
	private List<DiasDeUti> diasInternacoes;

	private double pesoNascimento;

	private double pesoAnteriar;

	private double pesoAtual;

	//@NotNull(message = "Selecione o setor relativo ao paciente.")
	@ManyToOne
	@JoinColumn(name = "id_setor_fk")
	private Setor setor;

	private int idade;// primeiro ano de nascimento
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "paciente")
	private List<BalancoHidrico> balancoHidrico;

	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "paciente")
	// private List<PrescricaoReceituario> prescricaoReceituario;

	public Paciente() {
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public List<IdadeGestacional> getIdadeGestacional() {
		return idadeGestacional;
	}

	public void setIdadeGestacional(List<IdadeGestacional> idadeGestacional) {
		this.idadeGestacional = idadeGestacional;
	}

	public List<DiasDeUti> getDiasInternacoes() {
		return diasInternacoes;
	}

	public void setDiasInternacoes(List<DiasDeUti> diasInternacoes) {
		this.diasInternacoes = diasInternacoes;
	}

	public double getPesoNascimento() {
		return pesoNascimento;
	}

	public void setPesoNascimento(double pesoNascimento) {
		this.pesoNascimento = pesoNascimento;
	}

	public double getPesoAnteriar() {
		return pesoAnteriar;
	}

	public void setPesoAnteriar(double pesoAnteriar) {
		this.pesoAnteriar = pesoAnteriar;
	}

	public double getPesoAtual() {
		return pesoAtual;
	}

	public void setPesoAtual(double pesoAtual) {
		this.pesoAtual = pesoAtual;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}
	
	public List<BalancoHidrico> getBalancoHidrico() {
		return balancoHidrico;
	}
	
	public void setBalancoHidrico(List<BalancoHidrico> balancoHidrico) {
		this.balancoHidrico = balancoHidrico;
	}

//	public int getProntuario() {
//		return prontuario;
//	}
//
//	public void setProntuario(int prontuario) {
//		this.prontuario = prontuario;
//	}

}
