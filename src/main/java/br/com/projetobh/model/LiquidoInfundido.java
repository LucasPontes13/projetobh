package br.com.projetobh.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@SuppressWarnings("serial")
@Entity
@Table(name = "tbl_liqInfundido")
public class LiquidoInfundido extends AbstractEntity<Long> {

	private Double sm;
	private Double voLmLa;
	private Double sondaLmLa;
	private Double soro;
	private Double npt;
	private Double hemo;
	private Double flush;

	// atributo para o calculo parcial
	private Double parcial;
	
	@Column(nullable = false, length = 2)
	@Enumerated(EnumType.STRING)
	private TURNO turno;

	@DateTimeFormat(iso = ISO.DATE)
	@Column(name = "data_cadastro", nullable = false, columnDefinition = "DATE")
	private LocalDate dataCadastro;

	private LocalTime horaCadastro;

	@ManyToOne
	@JoinColumn(name = "id_bh_fk")
	private BalancoHidrico balancoHidrico;

	public Double getSm() {
		return sm;
	}

	public void setSm(Double sm) {
		this.sm = sm;
	}

	public Double getVoLmLa() {
		return voLmLa;
	}

	public void setVoLmLa(Double voLmLa) {
		this.voLmLa = voLmLa;
	}

	public Double getSondaLmLa() {
		return sondaLmLa;
	}

	public void setSondaLmLa(Double sondaLmLa) {
		this.sondaLmLa = sondaLmLa;
	}

	public Double getSoro() {
		return soro;
	}

	public void setSoro(Double soro) {
		this.soro = soro;
	}

	public Double getNpt() {
		return npt;
	}

	public void setNpt(Double npt) {
		this.npt = npt;
	}

	public Double getHemo() {
		return hemo;
	}

	public void setHemo(Double hemo) {
		this.hemo = hemo;
	}

	public Double getFlush() {
		return flush;
	}

	public void setFlush(Double flush) {
		this.flush = flush;
	}

	public Double getParcial() {
		return parcial;
	}

	public void setParcial(Double parcial) {
		this.parcial = parcial;
	}
	
	public TURNO getTurno() {
		return turno;
	}
	
	public void setTurno(TURNO turno) {
		this.turno = turno;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public LocalTime getHoraCadastro() {
		return horaCadastro;
	}

	public void setHoraCadastro(LocalTime horaCadastro) {
		this.horaCadastro = horaCadastro;
	}

	public BalancoHidrico getBalancoHidrico() {
		return balancoHidrico;
	}

	public void setBalancoHidrico(BalancoHidrico balancoHidrico) {
		this.balancoHidrico = balancoHidrico;
	}

//	public double calculoParcial() {
//		double total = +sm;
//		return total;
//	}

/******************************************************************************************/
//	public Double getSomaParcial(List<LiquidoInfundido> lista) {
//		Double soma = 0.0;
//		
//		for (int i = 0; i < lista.size(); i++) {
//			soma += lista.get(i).getSm();
//		}
//		return soma;
//	}

}
