package br.com.projetobh.model;

public enum TURNO {
	
	MA("MA", "Manha"),
	TA("TA", "Tarde"),
	NO("NO", "Noite");
	
	private String sigla;
	private String descricao;
	
	TURNO(String sigla, String descricao) {
		this.sigla = sigla;
		this.descricao = descricao;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
