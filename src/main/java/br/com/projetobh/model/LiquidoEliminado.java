package br.com.projetobh.model;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@SuppressWarnings("serial")
@Entity
@Table(name = "tbl_liqEliminado")
public class LiquidoEliminado extends AbstractEntity<Long> {

	private Double urina;
	private Double fezes;
	private Double vomito;
	private Double dreno;
	private Double estase;
	
	//atributo para o calculo parcial
	private Double parcial;
	
	@Column(nullable = false, length = 2)
	@Enumerated(EnumType.STRING)
	private TURNO turno;

	@DateTimeFormat(iso = ISO.DATE)
	@Column(name = "data_cadastro", nullable = false, columnDefinition = "DATE")
	private LocalDate dataCadastro;
	
	private LocalTime horaCadastro;

	@ManyToOne
	@JoinColumn(name = "id_bh_fk")
	private BalancoHidrico balancoHidrico;

	public Double getUrina() {
		return urina;
	}

	public void setUrina(Double urina) {
		this.urina = urina;
	}

	public Double getFezes() {
		return fezes;
	}

	public void setFezes(Double fezes) {
		this.fezes = fezes;
	}

	public Double getVomito() {
		return vomito;
	}

	public void setVomito(Double vomito) {
		this.vomito = vomito;
	}

	public Double getDreno() {
		return dreno;
	}

	public void setDreno(Double dreno) {
		this.dreno = dreno;
	}

	public Double getEstase() {
		return estase;
	}

	public void setEstase(Double estase) {
		this.estase = estase;
	}

	public Double getParcial() {
		return parcial;
	}

	public void setParcial(Double parcial) {
		this.parcial = parcial;
	}
	
	public TURNO getTurno() {
		return turno;
	}
	
	public void setTurno(TURNO turno) {
		this.turno = turno;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public LocalTime getHoraCadastro() {
		return horaCadastro;
	}

	public void setHoraCadastro(LocalTime horaCadastro) {
		this.horaCadastro = horaCadastro;
	}

	public BalancoHidrico getBalancoHidrico() {
		return balancoHidrico;
	}

	public void setBalancoHidrico(BalancoHidrico balancoHidrico) {
		this.balancoHidrico = balancoHidrico;
	}

/******************************************************************************************/
	public Double somaParcialEliminado() {
		//essa linha está errada
		parcial = this.urina + this.fezes + this.vomito + this.dreno + this.estase;
		return parcial;
	}
}