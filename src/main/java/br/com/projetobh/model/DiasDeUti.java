package br.com.projetobh.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
@Table(name = "DiasDeUti_internacao")
public class DiasDeUti extends AbstractEntity<Long>{


	@DateTimeFormat(iso = ISO.DATE)
	@Column(name = "data_entrada", nullable = false, columnDefinition = "DATE")
	private LocalDate dataEntrada;

	@DateTimeFormat(iso = ISO.DATE)
	@Column(name = "data_saida", columnDefinition = "DATE")
	private LocalDate dataSaida;

	@DateTimeFormat(iso = ISO.DATE)
	@Column(name = "ataHoraInternacao", columnDefinition = "DATE")
	private LocalDate dataHoraInternacao;

	@ManyToOne
	@JoinColumn(name = "id_pessoa")
	private Paciente paciente;

	public DiasDeUti(){
	}

	public LocalDate getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(LocalDate dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public LocalDate getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(LocalDate dataSaida) {
		this.dataSaida = dataSaida;
	}

	public LocalDate getDataHoraInternacao() {
		return dataHoraInternacao;
	}

	public void setDataHoraInternacao(LocalDate dataHoraInternacao) {
		this.dataHoraInternacao = dataHoraInternacao;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
}
