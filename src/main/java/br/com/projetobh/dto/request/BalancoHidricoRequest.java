package br.com.projetobh.dto.request;

import br.com.projetobh.model.BalancoHidrico;
import br.com.projetobh.model.LiquidoEliminado;
import br.com.projetobh.model.LiquidoInfundido;
import br.com.projetobh.model.Paciente;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.List;

public class BalancoHidricoRequest {

    private String nomeBh; //será utilizado esse nome para associar ao paciente
    private Paciente paciente;

    private List<LiquidoInfundido> liquidoInfundido;

    private List<LiquidoEliminado> liquidoEliminado;

    private LocalDate dataCadastro;

    public BalancoHidricoRequest(String nomeBh, Paciente paciente, List<LiquidoInfundido> liquidoInfundido,
                                 List<LiquidoEliminado> liquidoEliminado, LocalDate dataCadastro) {
        this.nomeBh = nomeBh;
        this.paciente = paciente;
        this.liquidoInfundido = liquidoInfundido;
        this.liquidoEliminado = liquidoEliminado;
        this.dataCadastro = LocalDate.now();
    }

    public String getNomeBh() {
        return nomeBh + " - " + paciente.getNomeCompleto();
    }

    public void setNomeBh(String nomeBh) {
        this.nomeBh = nomeBh;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public List<LiquidoInfundido> getLiquidoInfundido() {
        return liquidoInfundido;
    }

    public void setLiquidoInfundido(List<LiquidoInfundido> liquidoInfundido) {
        this.liquidoInfundido = liquidoInfundido;
    }

    public List<LiquidoEliminado> getLiquidoEliminado() {
        return liquidoEliminado;
    }

    public void setLiquidoEliminado(List<LiquidoEliminado> liquidoEliminado) {
        this.liquidoEliminado = liquidoEliminado;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public BalancoHidrico toModel() {
        return new BalancoHidrico(this.nomeBh, this.paciente, this.liquidoInfundido,
                this.liquidoEliminado, this.dataCadastro);
    }
}
